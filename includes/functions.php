<?php
function cde_add_date_time_scripts(){
  wp_register_script(
    'cdash-add-events-datetimepicker',
    CDASH_EVENTS_URL.'/assets/jquery-timepicker-addon/jquery-ui-timepicker-addon.min.js',
    array('jquery')
  );

  wp_enqueue_script('cdash-add-events-datetimepicker');

  $lang = str_replace('_', '-', get_locale());
  $lang_exp = explode('-', $lang);

  if(file_exists(CDASH_EVENTS_PATH.'assets/jquery-timepicker-addon/i18n/jquery-ui-timepicker-'.$lang.'.js'))
    $lang_path = CDASH_EVENTS_URL.'/assets/jquery-timepicker-addon/i18n/jquery-ui-timepicker-'.$lang.'.js';
  elseif(file_exists(CDASH_EVENTS_PATH.'assets/jquery-timepicker-addon/i18n/jquery-ui-timepicker-'.$lang_exp[0].'.js'))
    $lang_path = CDASH_EVENTS_URL.'/assets/jquery-timepicker-addon/i18n/jquery-ui-timepicker-'.$lang_exp[0].'.js';

  if(isset($lang_path))
  {
    wp_register_script(
      'cdash-add-events-datetimepicker-localization',
      $lang_path,
      array('jquery', 'cdash-add-events-datetimepicker')
    );

    wp_enqueue_script('cdash-add-events-datetimepicker-localization');
  }

  /*wp_register_script(
    'cdash-add-events',
    CDASH_EVENTS_URL.'/js/admin-post.js',
    array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker', 'jquery-ui-slider', 'cdash-add-events-datetimepicker')
  );

  wp_enqueue_script('cdash-add-events');

  wp_localize_script(
    'cdash-add-events',
    'emPostArgs',
    array(
      'ticketsFields' => $this->tickets_fields,
      'ticketDelete' => __('Delete', 'cdash-events'),
      'currencySymbol' => cde_get_currency_symbol(),
      'startDateTime' => __('Start date/time', 'cdash-events'),
      'endDateTime' => __('End date/time', 'cdash-events'),
      'dateDelete' => __('Delete', 'cdash-events'),
      'deleteTicket' => __('Are you sure you want to delete this ticket?', 'cdash-events'),
      'deleteCustomOccurrence' => __('Are you sure you want to delete this occurrence?', 'cdash-events'),
      'firstWeekDay' => $this->options['general']['first_weekday'],
      'monthNames' => array_values($wp_locale->month),
      'monthNamesShort' => array_values($wp_locale->month_abbrev),
      'dayNames' => array_values($wp_locale->weekday),
      'dayNamesShort' => array_values($wp_locale->weekday_abbrev),
      'dayNamesMin' => array_values($wp_locale->weekday_initial),
      'isRTL' => $wp_locale->is_rtl(),
      'day' => __('day', 'cdash-events'),
      'days' => __('days', 'cdash-events'),
      'week' => __('week', 'cdash-events'),
      'weeks' => __('weeks', 'cdash-events'),
      'month' => __('month', 'cdash-events'),
      'months' => __('months', 'cdash-events'),
      'year' => __('year', 'cdash-events'),
      'years' => __('years', 'cdash-events')
    )
  );*/

  wp_localize_script( 'cdash-add-events', 'eventajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

  wp_register_style(
    'cdash-add-member-events',
    CDASH_EVENTS_URL.'/css/admin.css'
  );

  wp_register_style(
    'cdash-add-events-datetimepicker',
    CDASH_EVENTS_URL.'/assets/jquery-timepicker-addon/jquery-ui-timepicker-addon.min.css'
  );

  wp_enqueue_style('cdash-add-member-events');
  wp_enqueue_style('cdash-events-wplike');
  wp_enqueue_style('cdash-add-events-datetimepicker');

  
}

?>
