<?php
if(!defined('ABSPATH')) exit;

new Cdash_Events_Settings($cdash_events);

class Cdash_Events_Settings
{
	private $defaults = array();
	private $pages = array();
	private $errors = array();
	private $options = array();
	private $sortings = array();
	private $calendar_displays = array();
	private $calendar_contents = array();
	private $tabs = array();
	private $cdash_events;
	private $transient_id = '';


	public function __construct($cdash_events)
	{
		// passed vars
		$this->events_maker = $cdash_events;
		$this->options = $cdash_events->get_options();
		$this->defaults = $cdash_events->get_defaults();
		$this->transient_id = $cdash_events->get_session_id();

		//actions
		add_action('admin_menu', array($this, 'settings_page'));
		add_action('admin_init', array($this, 'register_settings'));
		add_action('after_setup_theme', array($this, 'load_defaults'));
		add_action( 'admin_enqueue_scripts', array($this, 'cdash_events_options_scripts') );
		//add_action( 'wp_enqueue_scripts', array($this, 'cdash_events_options_scripts') );
		add_action( 'cdash_settings_tab', array($this, 'cdash_calendar_tab'), 40 );
		add_action( 'cdash_settings_content', array($this, 'cdash_events_calendar_settigs') );

		//filters
		add_filter('plugin_action_links', array($this, 'plugin_settings_link'), 10, 2);
	}

	/**
	 *
	*/
	public function load_defaults()
	{
		$this->sortings = array(
			'publish' => __('Publish date', 'cdash-events'),
			'start' => __('Events start date', 'cdash-events'),
			'end' => __('Events end date', 'cdash-events')
		);

		$this->calendar_displays = array(
			'page' => __('selected page', 'cdash-events'),
			'manual' => __('manually', 'cdash-events')
		);

		$this->calendar_contents = array(
			'before' => __('before the content', 'cdash-events'),
			'after' => __('after the content', 'cdash-events')
		);

		$this->pages = get_pages(
			array(
				'sort_column' => 'post_title',
				'sort_order' => 'asc',
				'number' => 0
			)
		);

		$this->errors = apply_filters('cde_settings_errors', array(
			'settings_gene_saved' => __('General settings saved.', 'cdash-events'),
			'settings_gene_reseted' => __('General settings restored to defaults.', 'cdash-events'),
			'no_such_menu' => __('There is no such menu.', 'cdash-events'),
			'empty_menu_name' => __('Menu name can not be empty.', 'cdash-events')
		));

		$this->tabs = apply_filters('cde_settings_tabs', array(
			'general' => array(
				'name' => __('General', 'cdash-events'),
				'key' => 'cdash_events_general',
				'submit' => 'save_cde_general',
				'reset' => 'reset_cde_general'
			),
		));
	}


	public function cdash_events_options_scripts(){
		wp_enqueue_style('wp-color-picker');

		wp_enqueue_script(
	        'iris',
			CDASH_EVENTS_URL.'/js/iris.min.js',
	        array( 'jquery-ui-draggable', 'jquery-ui-slider', 'jquery-touch-punch' ),
	        false,
	        1
	    );
	    wp_enqueue_script(
	        'wp-color-picker',
			CDASH_EVENTS_URL.'/js/color.js',
	        array( 'iris' ),
	        false,
	        1
	    );
	    $colorpicker_l10n = array(
	        'clear' => __( 'Clear' ),
	        'defaultString' => __( 'Default' ),
	        'pick' => __( 'Select Color' ),
	        'current' => __( 'Current Color' ),
	    );
	    wp_localize_script( 'wp-color-picker', 'wpColorPickerL10n', $colorpicker_l10n );
		wp_enqueue_script(
			'cdash-events-options',
			CDASH_EVENTS_URL.'/js/options.js',
			array('jquery', 'wp-color-picker')
		);
	}


	/**
	 * Adds link to Settings page
	*/
	public function plugin_settings_link($links, $file)
	{
		if(!is_admin() || !current_user_can('manage_options'))
			return $links;

		static $plugin;

		$plugin = plugin_basename(__FILE__);

		if($file == $plugin)
		{
			$settings_link = sprintf('<a href="%s">%s</a>', admin_url('options-general.php').'?page=cd-settings&tab=cdash_ec', __('Settings', 'cdash-events'));
			array_unshift($links, $settings_link);
		}

		return $links;
	}


	/**
	 * Adds options page
	*/
	public function settings_page()
	{
		//add_submenu_page( '/chamber-dashboard-business-directory/options.php', __('Calendar Options', 'cdash-events'), __('Calendar Options', 'cdash-events'), 'manage_options', 'cdash-events', array($this, 'options_page') );

	}

	public function cdash_calendar_tab(){
		global $cdash_active_tab; ?>
	    <a class="nav-tab <?php echo $cdash_active_tab == 'cdash_ec' ? 'nav-tab-active' : ''; ?>" href="<?php echo admin_url( 'admin.php?page=cd-settings&tab=cdash_ec' ); ?>"><?php _e( 'Events Calendar', 'cdash' ); ?> </a>
		<?php
	}

	public function cdash_events_calendar_settigs(){
		global $cdash_active_tab;
		switch($cdash_active_tab){
			case 'cdash_ec':
				$this->options_page();
			break;
		}
	}

	/**
	 *
	*/
	public function options_page()
	{
		//$tab_key = (isset($_GET['tab']) ? $_GET['tab'] : 'general');
		$tab_key = 'general';
		?>
		<div class="wrap">
			<?php
	        $page = $_GET['page'];
	        if(isset($_GET['tab'])){
	            $tab = $_GET['tab'];
	        }
	        if(isset($_GET['section'])){
	            $section = $_GET['section'];
	        }else{
	            $section = "cdash_ec_settings";
	        }
	        ?>
			<h1><?php echo __('Chamber Dashboard Events Calendar', 'cdash-events'); ?></h1>
			<div id="main" class="cd_settings_tab_group" style="width: 100%; float: left;">
				<div class="cdash section_group">
	                <ul>
	                    <li class="<?php echo $section == 'cdash_ec_settings' ? 'section_active' : ''; ?>">
	                        <a href="?page=cd-settings&tab=cdash_ec&section=cdash_ec_settings" class="<?php echo $section == 'cdash_ec_settings' ? 'section_active' : ''; ?>"><?php esc_html_e( 'CD Events Calendar Settings', 'cdash' ); ?></a><span>|</span>
	                    </li>
	                    <li class="<?php echo $section == 'cdash_ec_docs' ? 'section_active' : ''; ?>">
	                        <a href="?page=cd-settings&tab=cdash_ec&section=cdash_ec_docs" class="<?php echo $section == 'cdash_ec_docs' ? 'section_active' : ''; ?>"><?php esc_html_e( 'Calendar Shortcodes', 'cdash' ); ?></a>
	                    </li>
	                </ul>
	            </div>
				<div class="cdash_section_content">
	                <?php
	                if( $section == 'cdash_ec_settings' )
	                {
						?>
						<div class="cdash-events-settings cdash_plugin_settings">
							<p class="cdash_tips" style="font-style:italic; font-weight:bold;">Troubleshooting Tip: If your individual event listings are not showing up on the front end of your site, please go to Settings >> Permalinks and click save.</p>

						<form action="options.php" method="post">
							<?php
							wp_nonce_field('update-options');
							settings_fields($this->tabs[$tab_key]['key']);
							?>
							<div class="settings_sections">
							<?php
							do_settings_sections($this->tabs[$tab_key]['key']);
							?>
							</div>
							<p class="submit">
							<?php
							submit_button('', 'primary', $this->tabs[$tab_key]['submit'], false);

							echo ' ';

							if($this->tabs[$tab_key]['reset'] !== false)
								submit_button(__('Reset to defaults', 'cdash-events'), 'secondary', $this->tabs[$tab_key]['reset'], false);
								?>
										</p>
									</form>

								</div>
						<?php
	                }else if($section == 'cdash_ec_docs'){
						$this->cdash_events_shortcodes();
	                }
	              ?>
	            </div>
			</div>
		</div>
<?php
	}

	public function cdash_events_shortcodes(){
		?>

		<div id="sidebar">
			<div class="cdash_top_blocks">
				<div class="cdash_block">
					<h3><?php echo __('Display Events Calendar', 'cdash-events'); ?></h3>
					<p><span class="bold">[events_calendar]</span> - <?php echo __('displays the calendar on your page. The default view is the month view', 'cdash-events'); ?><br />
						<span class="bold"><?php echo __('Acepted Parameters:', 'cdash-events'); ?></span> view, category, show_past_events</p>
					<p><a target="_blank" href="https://chamberdashboard.com/docs/plugin-features/events-calendar/display-calendar/"><?php echo __('Events Calendar Shortcode Docs', 'cdash-events'); ?></a></p>
				</div>
				<div class="cdash_block">
					<h3><?php echo __('Display Events Calendar', 'cdash-events'); ?></h3>
					<p><span class="bold">[cde-google-map]</span> - <?php echo __('displays the events on a map.', 'cdash-events'); ?><br />
					</p>
				</div>
				<div class="cdash_block">
					<h3><?php echo __('Member Submitted Events', 'cdash-events'); ?></h3>
					<p><span class="bold">[cde_update_events]</span> - <?php echo __('displays the form to add events on the front end.', 'cdash-events'); ?><br />
					</p>
					<p><a target="_blank" href="https://chamberdashboard.com/docs/plugin-features/events-calendar/member-submitted-events/"><?php echo __('Member Submitted Events Docs', 'cdash-events'); ?></a></p>
				</div>
			</div>
		</div>
		<?php
	}

	/**
	 *
	*/
	public function register_settings()
	{
		// general
		register_setting('cdash_events_general', 'cdash_events_general', array($this, 'validate_general'));
		add_settings_section('cdash_events_general', __('General settings', 'cdash-events'), array($this, 'cdash_events_general_callback'), 'cdash_events_general');
		add_settings_field('cde_default_event_options', __('Event default options', 'cdash-events'), array($this, 'cde_default_event_options'), 'cdash_events_general', 'cdash_events_general');
		add_settings_field('cde_thumbnail_display_options', __('Featured image display options', 'cdash-events'), array($this, 'cde_thumbnail_display_options'), 'cdash_events_general', 'cdash_events_general');
		add_settings_field('cde_events_in_rss', __('RSS feed', 'cdash-events'), array($this, 'cde_events_in_rss'), 'cdash_events_general', 'cdash_events_general');
		add_settings_field('cde_return_to_events_url', __('Events Page URL', 'cdash-events'), array($this, 'cde_return_to_events_url'), 'cdash_events_general', 'cdash_events_general');
		add_settings_field('cde_return_to_events_url_text', __('Events Page URL Text', 'cdash-events'), array($this, 'cde_return_to_events_url_text'), 'cdash_events_general', 'cdash_events_general');
		add_settings_field('cde_events_overlay_in_calendar', __('Event overlay in calendar', 'cdash-events'), array($this, 'cde_events_overlay_in_calendar'), 'cdash_events_general', 'cdash_events_general');
		add_settings_field('cde_events_overlay_bck_color', __('Background Color of the event overlay', 'cdash-events'), array($this, 'cde_events_overlay_bck_color'), 'cdash_events_general', 'cdash_events_general');
		add_settings_field('cde_events_overlay_text_color', __('Event overlay Text Color', 'cdash-events'), array($this, 'cde_events_overlay_text_color'), 'cdash_events_general', 'cdash_events_general');
		add_settings_field('cde_events_disable_bootstrap', __('Disable Bootstrap in Events Calendar', 'cdash-events'), array($this, 'cde_events_disable_bootstrap'), 'cdash_events_general', 'cdash_events_general');

		// other
		add_settings_section('cdash_events_other', __('Date settings', 'cdash-events'), array($this, 'cdash_events_other_callback'), 'cdash_events_general');
		add_settings_field('cde_date_format', __('Date and time format', 'cdash-events'), array($this, 'cde_date_format'), 'cdash_events_general', 'cdash_events_other');
		add_settings_field('cde_first_weekday', __('First day of the week', 'cdash-events'), array($this, 'cde_first_weekday'), 'cdash_events_general', 'cdash_events_other');

		/*The following settings will be available only when Member Updater is installed and active*/
		add_settings_section('cdash_events_member_events', __('Member Events', 'cdash-events'), array($this, 'cdash_events_member_events_callback'), 'cdash_events_general');

		add_settings_field('cde_enable_member_event_update', __('Enable Members to add/edit events', 'cdash-events'), array($this, 'cde_enable_member_event_update'), 'cdash_events_general', 'cdash_events_member_events');
		add_settings_field('cde_update_events_url', __('Add/Update Events URL', 'cdash-events'), array($this, 'cde_update_events_url'), 'cdash_events_general', 'cdash_events_member_events');

		do_action('cde_after_register_settings');
	}

	public function cdash_events_general_callback() {
		echo __('<span class="desc"></span>', 'cdash-events');
	}

	public function cdash_events_other_callback(){
		echo __('<span class="desc"></span>', 'cdash-events');
	}

	public function cdash_events_member_events_callback(){
		echo __('<span class="desc"></span>', 'cdash-events');
	}


	/**
	 *
	*/
	public function cde_default_event_options()
	{
		$options = array(
			'google_map' => __('Display Google Map', 'cdash-events'),
			'display_location_details' => __('Display Location Details', 'cdash-events'),
			'price_tickets_info' => __('Display Tickets Info', 'cdash-events')
		);

		$options = apply_filters('cde_default_event_display_options', $options);
		$values = $this->options['general']['default_event_options'];

		echo '
		<div id="cde_default_event_options">
			<fieldset>';
			foreach($options as $key => $name)
			{
				?>
				<label for="cde_default_event_option_<?php echo $key; ?>">
					<input id="cde_default_event_option_<?php echo $key; ?>" type="checkbox" name="cdash_events_general[default_event_options][<?php echo $key; ?>]" <?php checked((isset($values[$key]) && $values[$key] !== '' ? $values[$key] : '0'), '1'); ?> /><?php echo $name; ?>
				</label><br />
				<?php
			}
		echo '
				<span class="description">'.__('Select default display options for single event (this can overriden for each event separately).', 'cdash-events').'</span>
			</fieldset>
		</div>';
	}

	/**
	 *
	*/
	public function cde_thumbnail_display_options()
	{
		$options = array(
			'single_thumbnail' => __('Display Featured Image on Single Event View', 'cdash-events'),
			'archive_thumbnail' => __('Display Featured Image on Category and Tag Archive Views', 'cdash-events'),
		);

		$options = apply_filters('cde_thumbnail_display_options', $options);
		$values = $this->options['general']['thumbnail_display_options'];

		echo '
		<div id="cde_thumbnail_display_options">
			<fieldset>';
			foreach($options as $key => $name)
			{
				?>
				<label for="cde_thumbnail_display_option_<?php echo $key; ?>">
					<input id="cde_thumbnail_display_option_<?php echo $key; ?>" type="checkbox" name="cdash_events_general[thumbnail_display_options][<?php echo $key; ?>]" <?php checked((isset($values[$key]) && $values[$key] !== '' ? $values[$key] : '0'), '1'); ?> /><?php echo $name; ?>
				</label><br />
				<?php
			}
		echo '
				<span class="description">'.__('Some themes will automatically display featured images - check these options if you want to display featured images, but your theme does not already display them.', 'cdash-events').'</span>
			</fieldset>
		</div>';
	}


	/**
	 *
	*/
	public function cde_events_in_rss()
	{
		echo '
		<div id="cde_events_in_rss">
			<fieldset>
				<input id="em-events-in-rss" type="checkbox" name="cdash_events_general[events_in_rss]" '.checked($this->options['general']['events_in_rss'], true, false).' /><label for="em-events-in-rss">'.__('Enable RSS feed', 'cdash-events').'</label>
				<br />
				<span class="description">'.__('Enable to include events in your website main RSS feed.', 'cdash-events').'</span>
			</fieldset>
		</div>';
	}

	public function cde_return_to_events_url()
	{
		if(isset($this->options['general']['return_to_events_url'])){
			$value = $this->options['general']['return_to_events_url'];
		}else{
			$value = "";
		}
		echo '
		<div id="cde_return_to_events_url">
			<fieldset>
				<input id="em_return_to_events_url" value="'.$value.'" type="url" name="cdash_events_general[return_to_events_url]"/>
				<br />
				<span class="description">'.__('Enter the url for your events page here. It will be displayed on the single event pages so that users can navigate back to the events page.', 'cdash-events').'</span>
			</fieldset>
		</div>';
	}

	public function cde_return_to_events_url_text()
	{
		if(isset($this->options['general']['return_to_events_url_text'])){
			$value = $this->options['general']['return_to_events_url_text'];
		}else{
			$value = "Return to Calendar";
		}
		echo '
		<div id="cde_return_to_events_url_text">
			<fieldset>
				<input id="em_return_to_events_url_text" value="'.$value.'" type="text" name="cdash_events_general[return_to_events_url_text]"/>
				<br />
				<span class="description">'.__('Enter the text you want to show for the Return to Calendar link on the single events page.', 'cdash-events').'</span>
			</fieldset>
		</div>';
	}

	public function cde_events_overlay_in_calendar()
	{
		if(isset($this->options['general']['events_overlay_in_calendar'])){
			$value = $this->options['general']['events_overlay_in_calendar'];
		}else{
			$value = 1;
		}
		echo '
		<div id="cde_events_overlay_in_calendar">
			<fieldset>
				<input id="em_events_overlay_in_calendar" type="checkbox" name="cdash_events_general[events_overlay_in_calendar]" '.checked($value, true, false).' />
				<label for="em-events_overlay_in_calendar">'.__('Enable event overlay in calendar', 'cdash-events').'</label>
				<br />
				<span class="description">'.__('Show event name and date in popup.', 'cdash-events').'</span>
			</fieldset>
		</div>';
	}

	public function cde_events_overlay_bck_color(){
		if(isset($this->options['general']['events_overlay_bck_color'])){
			$value = $this->options['general']['events_overlay_bck_color'];
		}else{
			$value = '#FFC107';
		}
		echo '
		<div id="cde_events_overlay_bck_color">
			<fieldset>
				<input id="em_events_overlay_bck_color" value="'.$value.'" type="text" name="cdash_events_general[events_overlay_bck_color]" />
				<br />
				<span class="description">'.__('Set the background color for the event overlay. (Please note that it may take a few minutes for the new color to show up on your calendar)', 'cdash-events').'</span>
			</fieldset>
		</div>';
	}

	public function cde_events_overlay_text_color(){
		if(isset($this->options['general']['events_overlay_text_color'])){
			$value = $this->options['general']['events_overlay_text_color'];
		}else{
			$value = '#000000';
		}
		echo '
		<div id="cde_events_overlay_text_color">
			<fieldset>
				<input id="em_events_overlay_text_color" value="'.$value.'" type="text" name="cdash_events_general[events_overlay_text_color]" />
				<br />
				<span class="description">'.__('Set the background color for the event overlay. (Please note that it may take a few minutes for the new color to show up on your calendar)', 'cdash-events').'</span>
			</fieldset>
		</div>';
	}


	public function cde_events_disable_bootstrap()
	{
		if(isset($this->options['general']['events_disable_bootstrap'])){
			$value = $this->options['general']['events_disable_bootstrap'];
		}else{
			$value = 0;
		}
		echo '
		<div id="cde_events_disable_bootstrap">
			<fieldset>
				<input id="em_events_disable_bootstrap" type="checkbox" name="cdash_events_general[events_disable_bootstrap]" value="1" '.checked( 1, $value, false ).' />
				<label for="em-events_disable_bootstrap">'.__('Disable the Bootstrap styles', 'cdash-events').'</label>
				<br />
				<span class="description">'.__('If you are having page dispaly issues with events calendar, try disabling the Bootstrap styles.', 'cdash-events').'</span>
			</fieldset>
		</div>';
	}

	/**
	 *
	*/
	public function cde_date_format()
	{
		echo '
		<div id="cde_date_format">
			<fieldset>
				<label for="em-date-format">'.__('Date', 'cdash-events').':</label> <input id="em-date-format" type="text" name="cdash_events_general[datetime_format][date]" value="'.esc_attr($this->options['general']['datetime_format']['date']).'" /> <code>'.date_i18n($this->options['general']['datetime_format']['date'], current_time('timestamp')).'</code>
				<br />
				<label for="em-time-format">'.__('Time', 'cdash-events').':</label> <input id="em-time-format" type="text" name="cdash_events_general[datetime_format][time]" value="'.esc_attr($this->options['general']['datetime_format']['time']).'" /> <code>'.date($this->options['general']['datetime_format']['time'], current_time('timestamp')).'</code>
				<br />
				<span class="description">'.__('Enter your preffered date and time formatting.', 'cdash-events').'</span>
			</fieldset>
		</div>';
	}


	/**
	 *
	*/
	public function cde_first_weekday()
	{
		global $wp_locale;

		echo '
		<div id="cde_first_weekday">
			<fieldset>
				<select name="cdash_events_general[first_weekday]">
					<option value="1" '.selected(1, $this->options['general']['first_weekday'], false).'>'.$wp_locale->get_weekday(1).'</option>
					<option value="7" '.selected(7, $this->options['general']['first_weekday'], false).'>'.$wp_locale->get_weekday(0).'</option>
				</select>
				<br />
				<span class="description">'.__('Select preffered first day of the week for the calendar display.', 'cdash-events').'</span>
			</fieldset>
		</div>';
	}

	public function cde_enable_member_event_update(){
		if(isset($this->options['general']['enable_member_event'])){
			$value = $this->options['general']['enable_member_event'];
		}else{
			$value = 0;
		}

		if(!cdash_check_mu_active()){
			$disabled = "disabled";
			$description = __("Chamber Dashboard Member Updater needs to be installed and active to be able to use this feature.", "cdash-events");
		}else{
			$disabled = "";
			$description = __("Enable members to add/edit events.", "cdash-events");
		}

		echo'
		<div id="cde_enable_member_event_update">
			<fieldset>
				<input id="em_enable_member_event_update" type="checkbox" name="cdash_events_general[enable_member_event]" value="1" '.checked( 1, $value, false ). $disabled.' />
				<label for="em_enable_member_event_update">'.__('Enable members to add/edit events', 'cdash-events').'</label>
				<br />
				<span class="description">'.$description.'</span>
			</fieldset>
		</div>';
	}

	function cde_update_events_url(){
		if(isset($this->options['general']['event_update_url'])){
			$value = $this->options['general']['event_update_url'];
		}else{
			$value = "";
		}
		if(!cdash_check_mu_active()){
			$disabled = "disabled";
			$description = __("Chamber Dashboard Member Updater needs to be installed and active to be able to use this feature.", "cdash-events");
		}else{
			$disabled = "";
			$description = __("Enter the url where your members can add/update events here. This will be the url of the page with the event_update shortcode.", "cdash-events");
		}

		$event_page_select_dropdown = cdash_display_page_select_dropdown('cdash_events_general[event_update_url]', 'em_update_events_url', $value, $disabled);
		echo $event_page_select_dropdown;
		echo '</div>';
	}

	/**
	 * Validates or resets general settings
	*/
	public function validate_general($input)
	{
		if(isset($_POST['save_cde_general']))
		{
			// date, time, weekday
			$input['datetime_format']['date'] = sanitize_text_field($input['datetime_format']['date']);
			$input['datetime_format']['time'] = sanitize_text_field($input['datetime_format']['time']);
			if(isset($input['cdash_events_general[return_to_events_url]'])){
				$input['cdash_events_general[return_to_events_url]'] = sanitize_text_field($input['cdash_events_general[return_to_events_url]']);
			}
			if(isset($input['cdash_events_general[return_to_events_url_title]'])){
				$input['cdash_events_general[return_to_events_url_title]'] = sanitize_text_field($input['cdash_events_general[return_to_events_url_title]']);
			}
			
			$input['first_weekday'] = (in_array($input['first_weekday'], array(1, 7)) ? (int)$input['first_weekday']: $this->defaults['general']['first_weekday']);

			if($input['datetime_format']['date'] === '')
				$input['datetime_format']['date'] = get_option('date_format');

			if($input['datetime_format']['time'] === '')
				$input['datetime_format']['time'] = get_option('time_format');

			// event default options
			$default_event_options = array();

			if (isset($input['default_event_options']))
			{
				foreach($input['default_event_options'] as $key => $value)
				{
					$default_event_options[$key] = (isset($input['default_event_options'][$key]) ? true : false);
				}
			}
			$input['default_event_options'] = $default_event_options;

			// thumbnail default options
			$thumbnail_display_options = array();

			if (isset($input['thumbnail_display_options']))
			{
				foreach($input['thumbnail_display_options'] as $key => $value)
				{
					$thumbnail_display_options[$key] = (isset($input['thumbnail_display_options'][$key]) ? true : false);
				}
			}
			$input['thumbnail_display_options'] = $thumbnail_display_options;

			// RSS feed
			$input['events_in_rss'] = (isset($input['events_in_rss']) ? true : false);

			// Event Popup
			$input['events_overlay_in_calendar'] = (isset($input['events_overlay_in_calendar']) ? true : false);

			// Validate Overlay Background Color
    		$input['events_overlay_bck_color'] = trim( strip_tags( stripslashes( $input['events_overlay_bck_color'] ) ) );

			// Validate Overlay Text Color
    		$input['events_overlay_text_color'] = trim( strip_tags( stripslashes( $input['events_overlay_text_color'] ) ) );
		}
		elseif(isset($_POST['reset_cde_general']))
		{
			$input = $this->defaults['general'];

			if(!$this->options['general']['display_page_notice'])
				$input['display_page_notice'] = false;

			//menu
			$input['event_nav_menu']['show'] = false;
			$input['event_nav_menu']['menu_id'] = $this->defaults['general']['event_nav_menu']['menu_id'];
			$input['event_nav_menu']['menu_name'] = $this->defaults['general']['event_nav_menu']['menu_name'];
			$input['event_nav_menu']['item_id'] = $this->update_menu();

			//datetime format
			$input['datetime_format'] = array(
				'date' => get_option('date_format'),
				'time' => get_option('time_format')
			);

			// Disable Bootstrap
			$input['events_disable_bootstrap'] = (isset($input['events_disable_bootstrap']) ? true : false);

			// Enable Member Event Updates
			$input['enable_member_event'] = (isset($input['enable_member_event']) ? true : false);

			sanitize_text_field($input['cdash_events_general[event_update_url]']);

			set_transient($this->transient_id, maybe_serialize(array('status' => 'updated', 'text' => $this->errors['settings_gene_reseted'])), 60);
		}

		return $input;
	}

}
?>
