<?php
if(!defined('ABSPATH')) exit;

new Cdash_Events_Update($cdash_events);

class Cdash_Events_Update
{
	private $defaults = array();
	private $cdash_events;


	public function __construct($cdash_events)
	{
		// defaults
		$this->defaults = $cdash_events->get_defaults();

		// main object
		$this->events_maker = $cdash_events;

		// actions
		add_action('init', array($this, 'check_update'));
	}


	/**
	 *
	*/
	public function check_update()
	{
		if(!current_user_can('manage_options'))
			return;

		// updating?
		if(isset($_POST['cdash_events_update'], $_POST['cdash_events_number']))
		{
			if($_POST['cdash_events_number'] === 'update_1')
			{
				if(is_multisite() && is_network_admin())
				{
					global $wpdb;

					$current_blog_id = $wpdb->blogid;
					$blogs_ids = $wpdb->get_col($wpdb->prepare('SELECT blog_id FROM '.$wpdb->blogs, ''));

					foreach($blogs_ids as $blog_id)
					{
						switch_to_blog($blog_id);

						$current_db_version = get_option('cdash_events_version', '1.2.2');

						// updates only not updated sites
						if(version_compare($current_db_version, CDASH_EVENTS_UPDATE_VERSION_1, '<='))
						{
							// runs update
							$this->update_1();

							// updates plugin version
							update_option('cdash_events_version', $this->defaults['version']);
						}
					}

					switch_to_blog($current_blog_id);
				}
				else
				{
					$this->update_1();

					// updates plugin version
					update_option('cdash_events_version', $this->defaults['version']);
				}

				$this->events_maker->display_notice(__('Database was succesfully updated. Enjoy new features!', 'cdash-events'), 'updated', true);
			}
		}

		$update_1_html = '
		<form action="" method="post">
			<input type="hidden" name="cdash_events_number" value="update_1"/>
			<p>'.__('Chamber Dashboard Events Calendar requires a database update. Make sure you backup your database and then click.', 'cdash-events').' <input type="submit" class="button button-primary button-small" name="cdash_events_update" value="'.__('Update', 'cdash-events').'"/></p>
		</form>';

		// is it multisite network page?
		if(is_multisite() && is_network_admin())
		{
			global $wpdb;

			$current_blog_id = $wpdb->blogid;
			$blogs_ids = $wpdb->get_col($wpdb->prepare('SELECT blog_id FROM '.$wpdb->blogs, ''));
			$update_required = false;

			foreach($blogs_ids as $blog_id)
			{
				switch_to_blog($blog_id);

				// gets current database version
				$current_db_version = get_option('cdash_events_version', '1.2.2');

				// new version?
				if(version_compare($current_db_version, $this->defaults['version'], '<'))
				{
					// is update 1 required?
					if(version_compare($current_db_version, CDASH_EVENTS_UPDATE_VERSION_1, '<='))
						$update_required = true;
					else
						// updates plugin version
						update_option('cdash_events_version', $this->defaults['version']);
				}
			}

			if($update_required)
				$this->events_maker->display_notice($update_1_html);

			switch_to_blog($current_blog_id);
		}
		else
		{
			// gets current database version
			$current_db_version = get_option('cdash_events_version', '1.2.2');
			// wp_die($current_db_version);

			// new version?
			if(version_compare($current_db_version, $this->defaults['version'], '<'))
			{
				// is update 1 required?
				if(version_compare($current_db_version, CDASH_EVENTS_UPDATE_VERSION_1, '<=')) {

					$this->events_maker->display_notice($update_1_html);
				} else {
					// updates plugin version
					update_option('cdash_events_version', $this->defaults['version']);
				}
			}
		}
	}


	/**
	 *
	*/
	public function update_1()
	{
		$events = cde_get_events();

		if(!empty($events))
		{
			foreach($events as $event)
			{
				// check if the event is recurring
				$event_recurrence = get_post_meta( $event->ID, '_event_recurrence', true );
				if( is_array( $event_recurrence ) ) {
					// if so, create child events
					$cde_helper = new Cdash_Events_Helper();
					$post_id = $event->ID;
					$start = get_post_meta( $post_id, '_event_start_date', true );
					$end = get_post_meta( $post_id, '_event_end_date', true );
					$type = $event_recurrence['type'];
					$repeat = $event_recurrence['repeat'];
					$until = $event_recurrence['until'];
					// limit the number of repeating events
					$today = date( 'Y-m-d' );
					if( $today <= $until ) {
						$until = date( 'Y-m-d', strtotime( '+1 year' ) );
					}
					$weekly_days = $event_recurrence['weekly_days'];
					$monthly_day_type = $event_recurrence['monthly_day_type'];

					$format = 'Y-m-d H:i:s';
					$occurrences = array();
					$diff = strtotime($end) - strtotime($start);
					$finish = strtotime($until);

					if($type === 'daily')
					{
						$repeat *= 86400;
						$current = strtotime($start);

						while($current <= $finish)
						{
							$occurrences[] = array('start' => date($format, $current), 'end' => date($format, $current + $diff));

							// creates new current date
							$current += $repeat;
						}
					}
					elseif($type === 'weekly')
					{
						$current = $start_date = strtotime($start);
						$weekdays = array();
						$repeat *= 7;
						$i = $counter = 0;
						$day = date('N', $current);

						foreach($weekly_days as $weekday)
						{
							$weekdays[] = $weekday - $day;
						}

						$number_of_days = count($weekdays);

						while($current <= $finish)
						{
							if(($more_days = ($weekdays[$i++] + $repeat * $counter)) >= 0)
							{
								// creates new current date
								$current = strtotime('+'.$more_days.' days', $start_date);

								if($current <= $finish)
									$occurrences[] = array('start' => date($format, $current), 'end' => date($format, $current + $diff));
							}

							if($i === $number_of_days)
							{
								$counter++;
								$i = 0;
							}
						}
					}
					elseif($type === 'monthly')
					{
						$current = strtotime($start);
						$start_date = date_parse($start);

						// is it day of week?
						if($monthly_day_type === 2)
						{
							// 1-7
							$day_of_week = date('N', $current);

							// 1-31 / 7 rounded down
							$which = (int)floor(date('j', $current) / 7);

							// time
							$diff_time = $start_date['second'] + $start_date['minute'] * 60 + $start_date['hour'] * 3600;
						}
						else
							$diff_time = 0;

						while($current <= $finish)
						{
							$occurrences[] = array('start' => date($format, $current + $diff_time), 'end' => date($format, $current + $diff + $diff_time));

							// current date
							$date = date_parse(date('Y-m-d', $current));

							// creates new current date
							if($start_date['day'] > 28)
							{
								$values = date('Y-m-t', strtotime('+'.$repeat.' months', strtotime($date['year'].'-'.$date['month'].'-01')));
								$values = explode('-', $values);

								if($values[2] < $date['day'])
									$current = strtotime($values[0].'-'.$values[1].'-'.$values[2]);
								else
									$current = strtotime($values[0].'-'.$values[1].'-'.$start_date['day']);
							}
							else
								$current = strtotime('+'.$repeat.' months', $current);

							if($monthly_day_type === 2)
							{
								// due to PHP 5.2 bugs lets do some craziness
								$year = date('Y', $current);
								$month = date('m', $current);
								$day_of_month = date('N', strtotime($year.'-'.$month.'-01'));

								if($day_of_month <= $day_of_week)
									$number = $day_of_week - $day_of_month + 1;
								else
									$number = $day_of_week - $day_of_month + 8;

								$number += (7 * $which);

								// is it valid date?
								while(!checkdate((int)$month, $number, $year))
								{
									$number -= 7;
								}

								$current = strtotime($year.'-'.$month.'-'.str_pad($number, 2, '0', STR_PAD_LEFT));
							}
						}
					}
					elseif($type === 'yearly')
					{
						$current = strtotime($start);

						while($current <= $finish)
						{
							$occurrences[] = array('start' => date($format, $current), 'end' => date($format, $current + $diff));

							// creates new current date
							$current = strtotime('+1 year', $current);
						}
					}

					$i = 0;

					if(!empty($occurrences))
					{

						foreach( $occurrences as $occurrence ) {

							if( "0" == $i ) { // don't make a copy of the first event
								$i++;
								continue;
							}

							$original_event = get_post( $post_id );

							$args = array(
								'comment_status' => $original_event->comment_status,
								'ping_status'    => $original_event->ping_status,
								'post_author'    => $original_event->post_author,
								'post_content'   => $original_event->post_content,
								'post_excerpt'   => $original_event->post_excerpt,
								'post_name'      => $original_event->post_name,
								'post_parent'    => $post_id,
								'post_password'  => $original_event->post_password,
								'post_status'    => $original_event->post_status,
								'post_title'     => $original_event->post_title,
								'post_type'      => $original_event->post_type,
								'post_featured_image' => $original_event->post_thumbnail,
							);

							$recurring_event = wp_insert_post( $args );

							// copy all of the categories, tags, and locations
							$taxonomies = get_object_taxonomies($original_event->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
							foreach ($taxonomies as $taxonomy) {
								$post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
								wp_set_object_terms($recurring_event, $post_terms, $taxonomy, false);
							}

							// copy all of the post meta data
							$meta = get_post_meta($post_id);

					        foreach ( $meta as $key => $values) {
					            foreach ($values as $value) {
					            	if(@unserialize($value) !== false) {
					            		$value = unserialize($value);
					            	}
					                add_post_meta( $recurring_event, $key, $value );
					            }
					        }

					        // now replace the post meta data with this occurrence dates and times
					        update_post_meta( $recurring_event, '_event_start_date', $occurrence['start'] );
					        update_post_meta( $recurring_event, '_event_end_date', $occurrence['end'] );

					        $i++;
						}
					}
				}
			}
		}
	}
}
?>
