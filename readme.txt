=== Chamber Dashboard Events Calendar ===
Contributors: cguntur, gwendydd, LisaK.social
Donate link: http://chamberdashboard.com/donate
Tags: event, events management, event management, events manager, event manager, event organizer, events calendar, widget, attendance, attendee, calendar, event registration, ticket, tickets, ticketing, bookings, Chamber of Commerce
Requires at least: 4.6
Tested up to: 5.7
Stable tag: 2.3.9
Requires PHP: 7.0
License: MIT License
License URI: http://opensource.org/licenses/MIT

Create a calendar of events and display it on your site.  A fork of the Events Maker plugin, modified to work with the Chamber Dashboard suite of plugins.

== Description ==

Create a calendar of events to display on your site.  Chamber Dashboard Events Calendar is a part of the Chamber Dashboard collection of plugins and themes designed to meet the needs of chambers of commerce.

This plugin requires that you have the [Chamber Dashboard Business Directory](https://wordpress.org/plugins/chamber-dashboard-business-directory/) installed.



For more information, check out plugin page at [Chamber Dashboard](http://chamberdashboard.com/) site.

= Chamber Dashboard Events Calendar features: =

* Create a calendar of events
* Create recurring events, including daily, monthly, yearly and custom occurrences
* Interactive Full Calendar display
* Event Categories and Tags
* Event Locations
* Google maps
* RSS feed support
* Events widgets
* Compatible with any theme that follows basic WordPress theme standards
* Enable the option for members to add their own events when you have the [Member Updater](https://chamberdashboard.com/downloads/member-updater/) plugin

== Documentation ==

To display your events calendar on your website, create a page with the following shortcode: [events_calendar]

For full instructions about how to use the plugin, go to [Chamber Dashboard Documentation](https://chamberdashboard.com/document/events-calendar-docs/)

If you have any questions about this plugin, you can read through our extensive documentation. To be able to submit a support ticket, beyond pre-sales questions, you must purchase a premium license or priority support.

If you want to create a directory of businesses associated with your organization, check out [Chamber Dashboard Business Directory](https://wordpress.org/plugins/chamber-dashboard-business-directory/)

If you want to track the people associated with businesses in your organization, check out [the Chamber Dashboard CRM](https://wordpress.org/plugins/chamber-dashboard-crm/) plugin!

== Installation ==
= Using The WordPress Dashboard =

1. Navigate to the \'Add New\' in the plugins dashboard
2. Search for \'chamber dashboard events calendar\'
3. Click \'Install Now\'
4. Activate the plugin on the Plugin dashboard

= Uploading in WordPress Dashboard =

1. Navigate to the \'Add New\' in the plugins dashboard
2. Navigate to the \'Upload\' area
3. Select `chamber-dashboard-events-calendar.zip` from your computer
4. Click \'Install Now\'
5. Activate the plugin in the Plugin dashboard

= Using FTP =

1. Download `chamber-dashboard-events-calendar.zip`
2. Extract the `chamber-dashboard-events-calendar` directory to your computer
3. Upload the `chamber-dashboard-events-calendar` directory to the `/wp-content/plugins/` directory
4. Activate the plugin in the Plugin dashboard

== Frequently Asked Questions ==
= How do I display the events calendar on my site? =
Create a page, and insert the following shortcode:
[events_calendar]

= My events are not showing up. I am getting a 404 Error? =
Go to Settings -> Permalinks and re-save your permalinks. That will usually solve the 404 error.

= Will it work with my theme? =
Probably!  It is designed to work with any theme that follows basic WordPress coding practices.

= I would like to submit a few feature suggestions. Where can I do that? =
We would love to hear from you. You can contact us at [chamberdashboard.com/contact](http://chamberdashboard.com/contact)

== Screenshots ==

1. Month View
2. Month view with event overlay
3. List view
4. Event Calendar options in the admin
5. Events widget setup
6. Events widget on the front end
7. Add events from the front end

== Changelog ==
= 2.3.9 =
* Added an events block

= 2.3.8 =
* Fixed the add event link on the member account page  

= 2.3.7 =
* Fixed event widget issues and some issues with the calendar display 

= 2.3.6 =
* Fixed some of the date dispaly issues and the error with deleting recurring events

= 2.3.5 =
* Fixed the add events link to show only when the Member events feature is enabled

= 2.3.4 =
* Fixed the errors on the calendar settings page

= 2.3.3 =
* Updated the settings pages

= 2.3.2 =
* Fixed a couple of issues with the add event form

= 2.3.1 =
* Members can now add their own events

= 2.3.0 =
* Updated the Full Calendar to v4
* Added customization options for the event overlay box

= 2.2.9 =
* Added the option to disable Bootstrap styles
* Fixed a couple of minor errors

= 2.2.8 =
* Fixed the Google maps api for event locations
* Event location image upload issue fixed

= 2.2.7 =
* Fixed a few minor styling and event widget issues

= 2.2.6 =
* List, day and week views added to the events calendar shortcode

= 2.2.5 =
* Updated the Full Calendar version

= 2.2.4 =
* Fixed bug that prevented business directory style sheets from loading

= 2.2.3 =
* Fixed some styling issues with the event calendar shortcode

= 2.2.2 =
* Added Return to Calendar link on single events page
* Added event details popup for the calendar

= 2.2.1 =
* Updated the list view to show only the days with events

= 2.2 =
* Fixed the maps not displaying and updating when event locations are updated

= 2.1 =
* Removed Freemius integration
* Fixed the issue of map not showing up on single event pages
* Added a place to enter the name for the ticket purchase url

= 2.0.1 =
* fixed bug in event list widget that caused events to display outside of widget
* Freemius integration

= 2.0 =
* changes to language files to facilitate translations
* totally revamped how recurring events work
* updated fullcalendar libraries
* calendar now has an option for list view
* iCal support: links on calendar and single event
* calendar widget obeys first day of week option

= 1.3 =
* Added category parameter to events_calendar shortcode
* Fixed bug where recurring events were duplicated on first day

= 1.2 =
* Events Calendar now requires Chamber Dashboard Business Directory
* moved currency settings to Business Directory settings page

= 1.0.4 =
* Fixed category colors not saving

= 1.0.2 =
* Fixed capabilities bug

= 1.0.1 =
* Fixed bug that hid "add new event"

= 1.0.3 =
* Fixed time entries
* other minor fixes

= 1.0.2 =
* Fixed capabilities bug

= 1.0.1 =
* Fixed bug that hid "add new event"

= 1.0 =
* Initial release
