<?php
/**
 * The template for displaying event block content
 *
 * Override this template by copying it to yourtheme/content-block-event.php
 *
 * @author 	Digital Factory
 * @package Events Maker/Templates
 * @since 	1.2.0
 */

if (!defined('ABSPATH')) exit; // Exit if accessed directly

global $post;

if ($args && is_array($args))
	extract($args);

// get events args and post object sent via cde_get_template()
if (!$args)
	return;
else
	$post = apply_filters('cde_block_event_post', $args[0]); // event post object
	$args = apply_filters('cde_block_event_args', $args[1]); // block or function args

// extra event classes
$enable_border = $args['enableBorder'];
$border_color = $args['borderColor'];
$border_thickness = $args['borderThickness'];
$border_style = $args['borderStyle'];
$border_radius = $args['borderRadius'];
$border_radius_units = $args['borderRadiusUnits'];
$title_font_size = $args['titleFontSize'];
$imageSize = $args['imageSize'];

if($enable_border == 1){
	//$border_style = '<style = "border: '.$border_color . ' '. '$border_thickness'. 'px' . $border_style .'" />';
	$border_style = "border: " . $border_color . " " . $border_thickness ."px" . " " . $border_style;
	$border_radius = "border-radius: " . $border_radius . $border_radius_units;
}else{
	$border_style = "";
	$border_radius = "";
}
		/**
		 * cde_before_block_event hook
		 */
		do_action('cde_before_block_event');
?>
	<div id="event-<?php the_ID(); ?>" class="single_event <?php echo $args['add']; ?>" style="<?php echo $border_style . "; " . $border_radius; ?>">
	<?php

		if($args['titlePosition'] == 'title_above'){
			//Display title above image	
			cde_event_title($title_font_size);
			cde_block_display_image($args, $post);

		}elseif($args['titlePosition'] == 'title_below'){
			//Display title below the image
			cde_block_display_image($args, $post);
			cde_event_title($title_font_size);

		}

		// event excerpt
		if (apply_filters('cde_show_block_event_excerpt', $args['show_event_excerpt']) == true) : ?>

			<p class="event-excerpt">

				<?php the_excerpt(); ?>

			</p>

		<?php endif; 
		
		if (apply_filters('cde_show_block_event_date', $args['show_date']) == 1){ 
			//cde_display_event_date();
			cde_display_event_date('', $date_args = array('format' => '')); 
		} 

		if (apply_filters('cde_show_block_event_location', $args['show_location']) == 1){
			cde_display_event_locations();
		}

		if (apply_filters('cde_show_block_event_categories', $args['show_categories']) == 1){
			cde_display_event_categories();
		}

		if (apply_filters('cde_show_block_event_tags', $args['show_tags']) == 1){
			cde_display_event_tags();
		}

		cde_display_event_occurrences();
		/**
		 * cde_after_block_event hook
		 */
		do_action('cde_after_block_event');
		?>

	</div>
