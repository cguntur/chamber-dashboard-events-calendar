import edit from './edit';

import { registerBlockType } from '@wordpress/blocks';
import { __ } from '@wordpress/i18n';
import { dateI18n, format, __experimentalGetSettings } from '@wordpress/date';
import { setState } from '@wordpress/compose';

 
registerBlockType( 'cdash-bd-blocks/events-calendar', {
    title: __('Chamber Dashboard Display Events', 'cdash-events'),
    icon: 'calendar-alt',
    category: 'cd-blocks',
    description: __('The events calendar block displays the event listings on your page', 'cdash-events'),
    example: {

    },
    supports: {
        // Declare support for block's alignment.
        // This adds support for all the options:
        // left, center, right, wide, and full.
        align: [ 'wide', 'full' ]
    },
    attributes: {
        align: {
            type: 'string',
            default: ''
        },
        textAlignment: {
            type: 'string',
            default: 'left',
        },
        cd_block:{
            type: 'string',
            default: 'yes',
        },
        postLayout: {
             type: 'string',
             default: 'grid3',
	    },
        format: {
            type: 'string',
            default: 'grid3',
        },
        minColWidth: {
            type: 'number',
            default: 250,
        },
        categories:{
            type: 'array',
            default: [],
        },
        category:{
            type: 'string',
            default: '',
        },
        locations:{
            type: 'array',
            default: [],
        },
        location:{
            type: 'string',
            default: '',
        },
        displayPostContent:{
            type:Boolean,
            default: true,        
        },
        text:{
            type: 'string',
            default: 'none',
        },
        singleLinkToggle: {
            type: 'boolean',
            default: true,
        },
        perPage:{
            type: 'number',
            default: -1,
        },
        order_by:{
            type: 'string',
            default: 'title',
        },
        order:{
            type: 'string',
            default: 'asc',
        },
        show_event_thumbnail:{
            type: 'boolean',
            default: true,
        },
        show_event_excerpt: {
            type: 'boolean',
            default: true,
        },
        show_location:{
            type: 'boolean',
            default: false,
        },
        show_categories:{
            type: 'boolean',
            default: false,
        },
        show_tags:{
            type: 'boolean',
            default: false,
        },
        show_date:{
            type: 'boolean',
            default: false,
        },
        show_past_events: {
            type: 'boolean',
            default: true,
        },
        show_occurrences: {
            type: 'boolean',
            default: true,
        },
        changeTitleFontSize:{
            type: 'boolean',
            default: false,
        },
        titleFontSize:{
            type: 'number',
            default: 16,
        },
        imageSize:{
            type: 'string',
            default: 'medium',
        },
        titlePosition:{
            type: 'string',
            default: 'title_above',
        },
        enableBorder: {
            type: 'boolean',
            default: false,
        },
        borderColor: {
            type: 'string',
            default: '#000000',
        },
        borderThickness: {
            type: 'number',
            default: 1,
        },
        borderStyle: {
            type: 'string',
            default: 'solid',
        },
        borderRadius: {
            type: 'number',
            default: 0,
        },
        borderRadiusUnits: {
            type: 'string',
            default: 'px',
        },
    },
    edit: edit,
    save: () => <div>Events Block!</div>,
} );