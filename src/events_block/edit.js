import ServerSideRender from '@wordpress/server-side-render';
import { __ } from '@wordpress/i18n';
import { SelectControl, 
    Toolbar,
    Button,
    Tooltip,
    PanelBody,
    PanelRow,
    FormToggle,
    ToggleControl,
    ToolbarGroup,
    Disabled, 
    RadioControl,
    RangeControl,
    FontSizePicker,
    ColorPicker,
    ColorPalette,
} from '@wordpress/components';

    import {
        RichText,
        AlignmentToolbar,
        BlockControls,
        BlockAlignmentToolbar,
        InspectorControls,
        InnerBlocks,
        withColors,
        PanelColorSettings,
        getColorClassName
    } from '@wordpress/block-editor'
    ;
import { withSelect, widthDispatch } from '@wordpress/data';

import { withState } from '@wordpress/compose';

const formatOptions = [
    { label: __('List', 'cdash-events'), value: 'list' },
    { label: __('2 Columns', 'cdash-events'), value: 'grid2' },
    { label: __('3 Columns', 'cdash-events'), value: 'grid3' },
    { label: __('4 Columns', 'cdash-events'), value: 'grid4' },
    { label: __('Responsive', 'cdash-events'), value: 'responsive' },
 ];
const textOptions = [
    { label: __('Excerpt', 'cdash-events'), value: 'excerpt' },
    { label: __('Description', 'cdash-events'), value: 'description' },
    { label: __('None', 'cdash-events'), value: 'none' },
 ];
const orderbyOptions = [
    { label: __('Title', 'cdash-events'), value: 'title' },
    { label: __('Publish Date', 'cdash-events'), value: 'date' },
    { label: __('Start Date', 'cdash-events'), value: 'start' },
    { label: __('End Date', 'cdash-events'), value: 'end' },
 ];

const orderOptions = [
    { label: __('Ascending', 'cdash-events'), value: 'asc' },
    { label: __('Descending', 'cdash-events'), value: 'desc' },
];

const imageSizeOptions = [
    { label: __('Auto', 'cdash-events'), value: 'auto' },
    { label: __('Small', 'cdash-events'), value: 'thumbnail' },
    { label: __('Medium', 'cdash-events'), value: 'medium' },
    { label: __('Large', 'cdash-events'), value: 'large' },
    { label: __('Full Width', 'cdash-events'), value: 'full' },
];
const categoryOptions = [
    { label: __('Select one or more event categories', 'cdash-events'), value: null }
];

const borderStyleOptions = [
    { label: __('Solid', 'cdash-events'), value: 'solid' },
    { label: __('Dotted', 'cdash-events'), value: 'dotted' },
    { label: __('Dashed', 'cdash-events'), value: 'dashed' },
    { label: __('Double', 'cdash-events'), value: 'double' },
];

const borderRadiusUnitOptions = [
    { label: 'px', value: 'px' },
    { label: '%', value: '%' }
];

wp.apiFetch({path: "/wp/v2/event-category?per_page=100"}).then(posts => {
    jQuery.each( posts, function( key, val ) {
        categoryOptions.push({label: val.name, value: val.id});
    });
}).catch( 

)

const eventLocationOptions = [
    { label: __('Select one or more Event Locations', 'cdash-events'), value: null }
];

wp.apiFetch({path: "/wp/v2/event-location?per_page=100"}).then(posts => {
    jQuery.each( posts, function( key, val ) {
        eventLocationOptions.push({label: val.name, value: val.id});
    });
}).catch( 

)

const titleFontSizes = [
    {
        name: __( 'Small', 'cdash-events' ),
        slug: 'small',
        size: 12,
    },
    {
        name: __( 'Medium', 'cdash-events' ),
        slug: 'medium',
        size: 18,
    },
    {
        name: __( 'Big', 'cdash-events' ),
        slug: 'big',
        size: 26,
    },
];
const titleFallbackFontSize = 16;

const titlePostionOptions = [
    { label: __('Above the Image', 'cdash-events'), value: 'title_above' },
    { label: __('Below the Image', 'cdash-events'), value: 'title_below' },
];

const edit = props => {
    const {attributes: {align, textAlignment, cd_block, postLayout, format, minColWidth, categories, category, locations, location, displayPostContent, text, singleLinkToggle, perPage, order_by, order, show_event_thumbnail, imageSize, titlePosition, show_event_excerpt, show_location, show_categories, show_tags, show_date, show_past_events, show_occurrences, changeTitleFontSize, titleFontSize, enableBorder, borderColor, borderThickness, borderStyle, borderRadius, borderRadiusUnits}, className, setAttributes } = props;

    const inspectorControls = (
        <InspectorControls key="inspector">
            <PanelBody title={ __( 'Formatting Options', 'cdash-events' )}>
                <PanelRow>
                    <SelectControl
                        label={ __('Events Layout', 'cdash-events')}
                        value={ format }
                        options= { formatOptions }
                        onChange={ ( nextValue ) =>
                            setAttributes( {  format :  nextValue } )
                        }
                    />
                </PanelRow>
                {  format === "responsive" &&(

                    <PanelRow>
                        <RangeControl
                            label={ __('Minimum width of the column', 'cdash-events')}
                            min={100 }
                            max={ 500 }
                            onChange={ ( value ) => setAttributes( { minColWidth: value} ) }
                            value={ minColWidth }
                            initialPosition = { 250 }
                            allowReset = "true"
                            help = { __('The unit is set in px', 'cdash-events')}
                        />
                    </PanelRow>
                ) }
                <PanelRow>
                    <RangeControl
                        label={ __('Number of Events per page', 'cdash-events')}
                        min={-1 }
                        max={ 50 }
                        onChange={ ( value ) => setAttributes( { perPage: value} ) }
                        value={ perPage }
                        initialPosition = { -1 }
                        allowReset = "true"
                    />
                </PanelRow>
                <PanelRow>
                    <ToggleControl
                        label={ __( 'Enable Border Styles', 'cdash-events' ) }
                        checked={ enableBorder }
                        onChange={ ( nextValue ) =>
                            setAttributes( { enableBorder:  nextValue } )
                        }
                    />
                </PanelRow>
                { enableBorder && (
                    <PanelColorSettings
                        title={ __( 'Color Settings', 'cdash-events' ) }
                        colorSettings={ [
                            {
                                value: borderColor,
                                onChange: ( colorValue ) => setAttributes( { borderColor: colorValue } ),
                                label: __( 'Border Color', 'cdash-events' ),
                            },
                        ] }
                    />
                )}
                { enableBorder &&
                (  
                    <PanelRow>
                        <RangeControl
                            label={ __('Border Thickness', 'cdash-events')}
                            min={1 }
                            max={ 10 }
                            onChange={ ( value ) => setAttributes( { borderThickness: value} ) }
                            value={ borderThickness }
                            initialPosition = { 1 }
                            allowReset = "true"
                        />
                    </PanelRow>
                ) }
                { enableBorder &&
                (
                    <SelectControl
                        label={ __('Border Style', 'cdash-events')}
                        value={borderStyle}
                        options= { borderStyleOptions }
                        onChange={ ( nextValue ) =>
                            setAttributes( {borderStyle:  nextValue } )
                        }
                    />
                )}
                { enableBorder &&
                (  
                    <PanelRow>
                        <RangeControl
                            label={ __('Border Radius', 'cdash-events')}
                            min={0 }
                            max={ 100 }
                            onChange={ ( value ) => setAttributes( { borderRadius: value} ) }
                            value={ borderRadius }
                            initialPosition = { 0 }
                            allowReset = "true"
                        />
                    </PanelRow>
                ) }
                { enableBorder &&
                (  
                    <SelectControl
                        label={ __('Border Radius Units', 'cdash-events') }
                        value={borderRadiusUnits}
                        options= { borderRadiusUnitOptions }
                        onChange={ ( nextValue ) =>
                            setAttributes( {borderRadiusUnits:  nextValue } )
                        }
                    />
                ) }
                <PanelRow>
                    <SelectControl
                        label={ __('Order By', 'cdash-events')}
                        value={order_by}
                        options= { orderbyOptions }
                        onChange={ ( nextValue ) =>
                            setAttributes( {order_by:  nextValue } )
                        }
                    />
                </PanelRow>
                <PanelRow>
                    <SelectControl
                        label={ __('Order', 'cdash-events', )}
                        value={order}
                        options= { orderOptions }
                        onChange={ ( nextValue ) =>
                            setAttributes( {order:  nextValue } )
                        }
                    />
                </PanelRow>
                <PanelRow>
                    <ToggleControl
                        label={ __( 'Group Event Occurrences', 'cdash-events' ) }
                        checked={ show_occurrences }
                        onChange={ ( nextValue ) =>
                            setAttributes( { show_occurrences: nextValue } )
                        }
                    />
                </PanelRow>
            </PanelBody>
            <PanelBody title={ __( 'Title & Image Options:', 'cdash-events' )} initialOpen={ false }>
                <PanelRow>
                    <ToggleControl
                        label={ __( 'Enable Custom Title Font size', 'cdash-events' ) }
                        checked={ changeTitleFontSize }
                        //onChange = {setDisplayAddressToggle}
                        onChange={ ( nextValue ) =>
                            setAttributes( { changeTitleFontSize:  nextValue } )
                        }
                    />
                </PanelRow>
                { changeTitleFontSize &&
					(  
                        <PanelRow>
                            <FontSizePicker
                                fontSizes={ titleFontSizes }
                                value={ titleFontSize }
                                fallbackFontSize={ titleFallbackFontSize }
                                withSlider= "true"
                                onChange={ ( nextValue ) =>
                                    setAttributes( {titleFontSize:  nextValue } )
                                }
                            />
                        </PanelRow>
					) }

                <PanelRow>
                    <SelectControl
                        label={ __('Title Position', 'cdash-events')}
                        value={ titlePosition }
                        options= { titlePostionOptions }
                        onChange={ ( nextValue ) =>
                            setAttributes( {  titlePosition :  nextValue } )
                        }
                    />
                </PanelRow>
                {
                    titlePosition != "title_over" &&
                    (
                        <PanelRow>
                            <SelectControl
                                label={ __('Image Size', 'cdash-events')}
                                value={ imageSize }
                                options= { imageSizeOptions }
                                onChange={ ( nextValue ) =>
                                    setAttributes( {  imageSize :  nextValue } )
                                }
                            />
                        </PanelRow>
                    )
                }
            </PanelBody>
            <PanelBody title={ __( 'Limit By:', 'cdash-events' )} initialOpen={ false }>
                <PanelRow>
                    <SelectControl 
                        multiple
                        className = "cdash_multi_select"
                        label = { __('Categories', 'cdash-events')}
                        value = {categories}
                        options = {categoryOptions}
                        onChange={ ( nextValue ) =>
                            setAttributes( { categories: nextValue } )
                        }
                    />
                </PanelRow>
                <PanelRow>
                    <SelectControl 
                        multiple
                        className = "cdash_multi_select"
                        label = { __('Locations', 'cdash-events')}
                        value = {locations}
                        options = {eventLocationOptions}
                        onChange={ ( nextValue ) =>
                            setAttributes( { locations: nextValue } )
                        }
                    />
                </PanelRow>
            </PanelBody>
            <PanelBody title={ __( 'Display Options', 'cdash-events' )} initialOpen={ false }>
                <PanelRow>
                    <ToggleControl
                        label={ __( 'Display Excerpt', 'cdash-events' ) }
                        checked={ show_event_excerpt }
                        onChange={ ( nextValue ) =>
                            setAttributes( { show_event_excerpt: nextValue } )
                        }
                    />
                </PanelRow>
                <PanelRow>
                    <ToggleControl
                        label={ __( 'Display Featured Image', 'cdash-events' ) }
                        checked={ show_event_thumbnail }
                        onChange={ ( nextValue ) =>
                            setAttributes( { show_event_thumbnail: nextValue } )
                        }
                    />
                </PanelRow>
                <PanelRow>
                    <ToggleControl
                        label={ __( 'Display Location', 'cdash-events' ) }
                        checked={ show_location }
                        onChange={ ( nextValue ) =>
                            setAttributes( { show_location: nextValue } )
                        }
                    />
                </PanelRow>
                <PanelRow>
                    <ToggleControl
                        label={ __( 'Display Categories', 'cdash-events' ) }
                        checked={ show_categories }
                        onChange={ ( nextValue ) =>
                            setAttributes( { show_categories: nextValue } )
                        }
                    />
                </PanelRow>
                <PanelRow>
                    <ToggleControl
                        label={ __( 'Display Tags', 'cdash-events' ) }
                        checked={ show_tags }
                        onChange={ ( nextValue ) =>
                            setAttributes( { show_tags: nextValue } )
                        }
                    />
                </PanelRow>
                <PanelRow>
                    <ToggleControl
                        label={ __( 'Show Past Events', 'cdash-events' ) }
                        checked={ show_past_events }
                        onChange={ ( nextValue ) =>
                            setAttributes( { show_past_events: nextValue } )
                        }
                    />
                </PanelRow>
                <PanelRow>
                    <ToggleControl
                        label={ __( 'Display Date', 'cdash-events' ) }
                        checked={ show_date }
                        onChange={ ( nextValue ) =>
                            setAttributes( { show_date: nextValue } )
                        }
                    />
                </PanelRow>
            </PanelBody>
        </InspectorControls> 
    );

    const alignmentControls = (
        <BlockControls>
            <AlignmentToolbar
                value={textAlignment}
                onChange={(newalign) => setAttributes({ textAlignment: newalign })}
            />
        </BlockControls>
    );
    return [
        <div className={ props.className }>
            { <ServerSideRender
                block="cdash-bd-blocks/events-calendar"
                attributes = {props.attributes}
            /> }
            { alignmentControls }
            { inspectorControls }
            <div className="cd_events">
            </div>
        </div>
    ];
};

export default edit;