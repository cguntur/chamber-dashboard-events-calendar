<?php 
//Events Block output rendering
if ( function_exists( 'register_block_type' ) ) {
    // Hook server side rendering into render callback
    register_block_type(
        'cdash-bd-blocks/events-calendar', [
            'render_callback' => 'cdashec_events_block_callback',
            'attributes'  => array(
                'align'  => array(
                    'type'  => 'string',
                    'default' => 'center',
                ),
                'textAlignment' =>  array(
                    'type'  =>  'string',
                    'default'   =>  'left',
                ),
                'cd_block'  => array(
                    'type'  => 'string',
                    'default' => 'yes',
                ),
                'postLayout'    => array(
                    'type'  => 'string',
                    'default'   => 'grid3',
                ),
                'format'    => array(
                    'type'  => 'string',
                    'default'   => 'grid3',
                ),
                'minColWidth'   => array(
                    'type'  =>  'number',
                    'default'   =>  '250'
                ),
                'categories'    => array(
                    'type'  => 'array',
                    'default'   => [],
                    'items'   => [
                        'type' => 'string',
                    ],
                ),
                'category'    => array(
                    'type'  => 'string',
                    'default'   => '',
                ),
                'locations'    => array(
                    'type'  => 'array',
                    'default'   => [],
                    'items'   => [
                        'type' => 'string',
                    ],
                ),
                'location'    => array(
                    'type'  => 'string',
                    'default'   => '',
                ),
                'displayPostContent'   => array(
                    'type'  => 'boolean',
                    'default'   => 'true',
                ),
                'text'    => array(
                    'type'  => 'string',
                    'default'   => 'none',
                ),
                'singleLinkToggle'    => array(
                    'type'  => 'boolean',
                    'default'   => 'true',
                ),
                'perPage'    => array(
                    'type'  => 'number',
                    'default'   => -1,
                ),
                'order_by'    => array(
                    'type'  => 'string',
                    'default'   => 'title',
                ),
                'order'    => array(
                    'type'  => 'string',
                    'default'   => 'asc',
                ),
                'show_event_thumbnail'    => array(
                    'type'  => 'boolean',
                    'default'   => 'true',
                ),
                'imageSize'    => array(
                    'type'  => 'string',
                    'default'   => 'medium',
                ),
                'titlePosition'  => array(
                    'type'  =>  'string',
                    'default' => 'title_above', 
                ),
                'show_event_excerpt'    => array(
                    'type'  => 'boolean',
                    'default'   => 'true',
                ),
                'show_location'    => array(
                    'type'  => 'boolean',
                    'default'   => 'false',
                ),
                'show_categories'    => array(
                    'type'  => 'boolean',
                    'default'   => 'false',
                ),
                'show_tags'    => array(
                    'type'  => 'boolean',
                    'default'   => 'false',
                ),
                'show_date'    => array(
                    'type'  => 'boolean',
                    'default'   => 'false',
                ),
                'show_past_events'    => array(
                    'type'  => 'boolean',
                    'default'   => 'true',
                ),
                'show_occurrences'    => array(
                    'type'  => 'boolean',
                    'default'   => 'true',
                ),
                'changeTitleFontSize'    => array(
                    'type'  => 'boolean',
                    'default'   => 'false',
                ),
                'titleFontSize'    => array(
                    'type'  => 'number',
                    'default'   => 16,
                ),
                'enableBorder'    => array(
                    'type'  => 'boolean',
                    'default'   => 'false',
                ),
                'borderColor'    => array(
                    'type'  => 'string',
                    'default'   => '#000000',
                ),
                'borderThickness'    => array(
                    'type'  => 'number',
                    'default'   => '1',
                ),
                'borderStyle'    => array(
                    'type'  => 'string',
                    'default'   => 'solid',
                ),
                'borderRadius'    => array(
                    'type'  => 'number',
                    'default'   => '0',
                ),
                'borderRadiusUnits'    => array(
                    'type'  => 'string',
                    'default'   => 'px',
                ),
            ),
        ]
    );
}

function cdashec_events_block_callback($attributes){
    //return "Events Block!";
    $events = "";
    $events .= cde_display_block_events($attributes);
    return $events;
}
?>