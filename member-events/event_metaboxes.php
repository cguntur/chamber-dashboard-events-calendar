<?php
if( defined( 'CDASH_PATH' ) ) {
    // Create the connection between events and people
    function cdashec_event_and_people() {
      if(cdash_check_mu_active()){
        p2p_register_connection_type( array(
          'name' => 'event_to_people',
          'from' => 'event',
          'to' => 'person',
          'reciprocal' => true,
          'admin_column' => 'from',
          'admin_box' => array(
        'context' => 'side'
        ),
        'title' => array(
        'from' => __( 'Connected People', 'cdash-events' ),
        'to' => __( 'Connected Events', 'cdash-events' )
        )
        ) );
      }
    }
      add_action( 'p2p_init', 'cdashec_event_and_people' );

    //Showing connected events in a metabox on the single person page
    //add_action( 'add_meta_boxes', 'cdashec_register_connected_events_meta_box' );

    function cdashec_register_connected_events_meta_box() {
      add_meta_box( 'connected-events-box', __('Connected Events'), 'cdashec_render_connected_events_meta_box', 'person' );
    }


    function cdashec_render_connected_events_meta_box( $post ) {
    	$connected = p2p_type( 'event_to_people' )->get_connected($post);
    ?>
    <ul>
    <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
       <?php $title = get_the_title(); ?>
        <li><?php edit_post_link($title); ?></li>
    <?php endwhile; ?>
    </ul>
    <?php
    }


    //Showing connected person in a metabox on the single event page
    //add_action( 'add_meta_boxes', 'cdashec_register_connected_people_meta_box' );

    function cdashec_register_connected_people_meta_box() {
    	add_meta_box( 'connected-people-box', __('Connected People'), 'cdashec_render_connected_people_meta_box', 'event' );
    }


    function cdashec_render_connected_people_meta_box( $post ) {
    	$connected = p2p_type( 'event_to_people' )->get_connected($post);
    ?>
    <ul>
    <?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
       <?php $title = get_the_title(); ?>
        <li><?php edit_post_link($title); ?></li>
    <?php endwhile; ?>
    </ul>
    <?php
    }



}



?>
