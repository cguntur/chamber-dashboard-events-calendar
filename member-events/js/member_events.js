jQuery(document).ready(function($) {
  $(".add_new_category").css("display", "none");

  /*$(".add_new_category_button").click(function (evt) {
    $(".add_new_category").css("display", "block");
});*/

  $(".add_new_location_button").click(function(evt){
    $(".location_fields.cdashec_hidden").css("display", "block");
});

  $("#event_free").click(function() {
    if (!$(this).is(':checked')) {
      //alert("Unchecked! The event has tickets.");
      $(".event_ticket_details").css("display", "block");
    }else{
      //alert("Checked! The event is free.")
      $(".event_ticket_details").css("display", "none");
    }
  });

  $('#add_event_all_day').change(function () {
     $('#event_start_time, #event_end_time').toggle(!this.checked);
     if ( $( '#add_event_all_day' ).is( ":checked" ) ){
      $('#event_start_time, #event_end_time').prop("required", false);
     }else{
      $('#event_start_time, #event_end_time').prop("required", true);
     }
  }).change(); //ensure visible state matches initially


  dateOptions = {
    dateFormat: 'yy-mm-dd',
    showButtonPanel: true
  }

  $( ".hasDatepicker" ).datepicker(dateOptions);

  timeOptions = {
    timeFormat: 'HH:mm',
    stepMinute: 5,
    controlType: 'select',
    timeOnly: true,
    timeInput: true
  };

  $(".hasTimepicker").timepicker(timeOptions);

  $( ".showaddress" ).tooltip({
      show: {
        effect: "slideDown",
        delay: 250
      }
    });


  /*$(window).on('load', function () {
  //alert("Window Loaded");
      $( ".hasDatepicker" ).datepicker({
          showOn: "button",
          buttonImage: "images/calendar.gif",
          buttonImageOnly: true,
          buttonText: "Select date"
      });
  });*/
});
