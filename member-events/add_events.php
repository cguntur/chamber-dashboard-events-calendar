<?php
// ------------------------------------------------------------------------
// FORM TO ADD/EDIT EVENTS
// ------------------------------------------------------------------------

function add_events_scripts(){
    global $wp_locale;

  wp_enqueue_script('cdash-events-admin-edit');
  wp_enqueue_style( 'cdash-add-events-css', CDASH_EVENTS_URL.'/css/add_events.css', '', null);
  wp_enqueue_style( 'cdash_member_events_css', CDASH_EVENTS_URL.'/member-events/css/member_events.css', '', null);

  wp_enqueue_script('jquery');
  wp_enqueue_script('jquery-ui');
  wp_register_script('jquery-ui-datepicker', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array('jquery'));
  wp_enqueue_script('jquery-ui-datepicker');

  wp_register_script('font-awesome', 'https://kit.fontawesome.com/991c67fc22.js');
  wp_enqueue_script('font-awesome');

  wp_register_style('jquery-ui-css', 'http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
  wp_enqueue_style( 'jquery-ui-css' );

  wp_register_script('cdashec_member_events_js', CDASH_EVENTS_URL.'/member-events/js/member_events.js', array('jquery', 'jquery-ui-core', 'jquery-ui-datepicker') );

  wp_enqueue_script('cdashec_member_events_js');
}
//add_action( 'wp_enqueue_scripts', 'add_events_scripts' );


// Register a new shortcode: [cde_update_events]
add_shortcode( 'cde_update_events', 'cde_submit_events' );

function cde_submit_events(){
  if(!cdash_check_mu_active()){
     $message =   __("<p>Member submitted events are not currently enabled. Please contact the site admin for more details. Thank you!</p>", 'cdash-events');
     return $message;
  }else{
    ob_start();
    cde_event_update_form();
    return ob_get_clean();
  }
}

function cde_event_update_form(){
  global $wp;
  add_events_scripts();
  cde_add_date_time_scripts();
  $event_options = get_option('cdash_events_general');
  if(!isset($event_content)){
    $event_content = "";
  }

  $user_id = "";
  $user_id = cdashmm_get_current_user_id();
  $member_options = get_option('cdashmu_options');
  $mm_options = get_option('cdashmm_options');
  $redirect_slug = cdashrc_get_redirect_slug();
  $login_page = cdashmu_login_page();
  $login_page_redirect = $login_page . '/?redirect=' . $redirect_slug;
  if(!$user_id){
      echo __("<p>Please login <a href='" . $login_page_redirect . "'>here</a> to add/update your event.</p>", "cdash-events");
      return;
  }
  // Get the user object.
  $user_meta = get_userdata( $user_id );
  
  // Get all the user roles as an array.
  $user_roles = $user_meta->roles;
  $person_id = cdashmm_get_person_id_from_user_id($user_id, false);
  if(!$person_id){
      $business_id = null;
  }else{
      $business_id = cdashmm_get_business_id_from_person_id($person_id, false);
  }

  if(!$business_id){
      if ( current_user_can( 'manage_options' ) ) {
            echo __("<p>You are logged in as the site administrator. To access a business listing, please login as the business editor.</p>", "cdash-events");
        }else{
            echo __("<p>You are not authorized to add/update events. Please contact the site administrator for more details.</p>", "cdash-events");
        }
  }else{
        cde_add_new_event_form($person_id, $business_id, $user_id);
  }
}

function cde_add_new_event_form($person_id, $business_id, $user_id){
  if(!isset($event_content)){
      $event_content = "";
  }
  if(isset($_POST['submit'])){
      $event_id = cde_save_event_form($person_id, $business_id, $user_id);
      if($event_id){
          $event_title = get_the_title($event_id);
          $event_permalink = get_permalink($event_id);
          echo '<p>' . __('Your event has been successfully added. Your event will show up on your Account page after it has been published by the admin.', 'cdash-events') . '<br />';
      }
    return;
  }
?>
  <form id="add_event_form" name="add_event_form" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" enctype="multipart/form-data">
    <p class="explain"><?php echo __( "* = Required", "cdash-events"); ?></p>

    <?php
    wp_nonce_field('register_nonce');
    ?>
    <fieldset>
      <legend><?php echo __("Event Details", "cdash-events"); ?></legend>
      <p>
    		<label><?php echo __( "Event Title", "cdash-events" ); ?> *</label>
    		<input name="event_name" class="" type="text" id="event_name" required>
    	</p>
      <p>
        <label><?php echo __( "Event Description", "cdash-events" ); ?> *</label>
        <?php
        $editor_id = 'event_desc';
        $settings = array( "wpautop" => true, 'editor_height' => '200', 'media_buttons' => false );
        wp_editor( '', $editor_id, $settings );
        ?>
      </p>
      <p>
        <label><?php echo __( "Featured Image", "cdash-events" ); ?></label>
        <input type="file" name="event_featured_image" id="event_featured_image_upload" value=""/>
      </p>
    </fieldset>
    <fieldset>
      <legend><?php echo __("Event Date and Time", "cdash-events"); ?></legend>
      <p class="event_start_date_time">
        <label for="event_start_date">Start Date & Time</label>
        <input  id="event_start_date" class="small_width format-date events-datepicker hasDatepicker" type="text" name="event_start_date" placeholder="yyyy-mm-dd" required/>
        <input  id="event_start_time" class="small_width format-time hasTimepicker" type="text" name="event_start_time"/>
      </p>

      <p class="event_end_date_time">
        <label for="event_end_date">End Date & Time</label>
        <input  id="event_end_date" class="small_width format-date events-datepicker hasDatepicker" type="text" name="event_end_date" placeholder="yyyy-mm-dd" />
        <input  id="event_end_time" class="small_width format-time hasTimepicker" type="text" name="event_end_time" />
      </p>

      <p>
        <label><?php echo __("All Day Event", "cdash-events"); ?></label>
        <input type="checkbox" name="add_event_all_day" id="add_event_all_day" checked="checked" />
      </p>

    </fieldset>
    <fieldset>
      <legend><?php echo __("Event Categories", "cdash-events"); ?></legend>
      <div>
          <p> <?php echo __('Please select the event category from the list below. ')?></p>
          <?php
          $taxonomy = 'event-category';
  		    $terms = get_terms( 'event-category', array(
  			       'hide_empty' => false,
  			  ) );
          foreach($terms as $single_category){
              $category_slug = $single_category->slug;
          ?>
              <input type="checkbox" name="event_category[]" value="<?php echo $single_category->slug; ?>"
              />
              <label><?php echo $single_category-> name; ?></label><br />
              <?php
          }
              ?>
          <br />
      </div>

    </fieldset>

    <fieldset>
      <legend><?php echo __("Event Location", "cdash-events"); ?></legend>
      <p><small><?php echo __("(Hover over the icon for the location's address.)", "cdash-events"); ?></small></p>
      <p>
          <?php
          $taxonomy = 'event-location';
          $terms = get_terms( array(
              'taxonomy' => 'event-location',
              'hide_empty' => false,
              'fields'     => 'all'
            ) );
          foreach($terms as $term){
              $term_id = $term->term_id;
              $category_slug = $term->slug;
              $term_link = get_term_link($term->slug, 'event-location');
              $location = cde_get_location($term->term_id);
              $location_address = '';
              if(isset($location->location_meta['address'])){
                  $location_address .= $location->location_meta['address'];
              }

              if(isset($location->location_meta['city'])){
                  $location_address .= ' ' . $location->location_meta['city'] . ',';
              }

              if(isset($location->location_meta['state'])){
                  $location_address .= ' ' . $location->location_meta['state'];
              }

              if(isset($location->location_meta['zip'])){
                  $location_address .= ' ' . $location->location_meta['zip'];
              }

              if(isset($location->location_meta['country'])){
                  $location_address .= ' ' . $location->location_meta['country'];
              }
              ?>
              <input type="checkbox" name="event_location[]" value="<?php echo $term->slug; ?>"
              />
              <label><?php echo $term->name . ' '; ?><span class="location_address"><a class="show_address" href="#" title="<?php echo $location_address; ?>"><i class="far fa-address-card"></i></a></span></label><br />
              <?php
          }
              ?>
      </p>
    </fieldset>

    <fieldset>
      <legend><?php echo __("Event Tickets", "cdash-events"); ?></legend>
      <p>
        <label for="cdash_free_event"><?php echo __("Is this a free event?", "cdash-events"); ?></label>
        <input id="event_free" type="checkbox" name="event_free" value="1" checked="checked" />
      </p>
      <div class="event_ticket_details">
        <p>
          <label for="event_tickets[0][name]"><?php echo __("Ticket Name:", "cdash-events"); ?></label><input class="small_width" type="text" id="event_tickets[0][name]" name="event_tickets[0][name]" />
          <br /><br />
          <label for="event_tickets[0][price]"><?php echo __("Ticket Price:", "cdash-events"); ?></label><input class="small_width" type="text" id="event_tickets[0][price]" name="event_tickets[0][price]" /><br />
        </p>
        <p>
          <span><?php echo __("Ticket Purchase Link (external site):", "cdash-events"); ?></span><br />
          <label><?php echo __("Purchase site:", "cdash-events"); ?></label>
          <input id="event_tickets_url_name" type="text" class="small_width" name="event_tickets_url_name" />
          <br /><br />
          <label><?php echo __("Purchase URL:", "cdash-events"); ?></label>
          <input id="event_tickets_url" type="url" class="small_width" name="event_tickets_url" />
        </p>
      </div>
    </fieldset>
    <fieldset>
        <legend><?php echo __("Notes to the admin:", "cdash-events"); ?></legend>
        <div>
            <p><textarea name="event_form_notes"></textarea></p>
        </div>
    </fieldset>
  <p>
    <input type="submit" name="submit" id="cde_add_event_form" value="Submit" />
	</p>

<?php

}

function cde_save_event_form($person_id, $business_id, $user_id){
  // check for the nonce and get out of here right away if it isn't right
  $retrieved_nonce = $_REQUEST['_wpnonce'];
  if (!wp_verify_nonce($retrieved_nonce, 'register_nonce')) die( 'Failed security check' );

    // gather all of the variables
    if(isset($_POST['event_name'])){
    $event_title = sanitize_text_field( $_POST['event_name'] );
    }else{
    $event_title = '';
    }

    if(isset($_POST['event_desc'])){
      $event_description = sanitize_textarea_field($_POST['event_desc']);
    }else{
      $event_description = '';
    }

    if(isset($_POST['event_featured_image'])){
        $featured_image = $_POST['event_featured_image'];
    }else{
        $featured_image = '';
    }
    $file = $_FILES['event_featured_image'];


  $event_details = array(
    'post_status' => 'draft',
    'post_type'   =>  'event',
    'post_title'  =>  $event_title,
    'post_content'  =>  $event_description
  );

  //create event and publish
  $event_id = wp_insert_post($event_details);

  //Upload and add the featured image
  cdashec_add_event_featured_image($event_id, $file);

  //Adding Event Categories
  $category_terms = [];
  if(isset($_POST['event_category'])){
    $category_terms = $_POST['event_category'];
  }
  wp_set_object_terms( $event_id, $category_terms, 'event-category', true);

  //Adding Event location
  global $cdash_events;
  $cdash_events_taxonomies = new Cdash_Events_Taxonomies($cdash_events);
  $location_terms = [];
  if(isset($_POST['event_location'])){
    $location_terms = $_POST['event_location'];
  }
  wp_set_object_terms( $event_id, $location_terms, 'event-location', true);

  //Adding Event Date Details
  $event_all_day = isset($_POST['add_event_all_day']) ? 1 : 0;
  $post_event_start_date = $_POST['event_start_date'];
  $post_event_end_date = $_POST['event_end_date'];
  $event_start_time = $_POST['event_start_time'];
  if($_POST['event_end_time'] != ''){
    $event_end_time = $_POST['event_end_time'];
  }else{
    $event_end_time = $event_start_time;
  }

  cdashec_add_event_date_time($event_id, $event_all_day, $post_event_start_date, $post_event_end_date, $event_start_time, $event_end_time);

  //Event Ticket Details
  $tickets = $_POST['event_tickets'];

  if(isset($_POST['event_tickets_url'])){
      $tickets_url = $_POST['event_tickets_url'];
  }else{
      $tickets_url = '';
  }

  if(isset($_POST['event_tickets_url_name'])){
      $tickets_url_name = $_POST['event_tickets_url_name'];
  }else{
      $tickets_url_name = '';
  }

  if(isset($_POST['event_free'])){
      $free_event = 1;
  }else{
      $free_event = 0;
  }

  cdashec_add_event_ticket_details($event_id, $free_event, $tickets, $tickets_url, $tickets_url_name);

  // connect the event to the person
	cdashec_connect_event_to_people($event_id, $person_id);
    if(isset($_POST['event_form_notes'])){
        $additional_notes = $_POST['event_form_notes'];
    }else{
        $additional_notes = '';
    }
    //Send admin email
    cdashec_send_admin_email($event_id, $business_id, $user_id, $additional_notes);

    return $event_id;

}


function cdashec_connect_event_to_people($event_id, $person_id) {
    p2p_type('event_to_people')->connect($person_id, $event_id, array('date' => current_time('mysql')));
}

function cdashec_add_event_featured_image($event_id, $file){

        require_once(ABSPATH . "wp-admin" . '/includes/image.php');
        require_once(ABSPATH . "wp-admin" . '/includes/file.php');
        require_once(ABSPATH . "wp-admin" . '/includes/media.php');

        $uploaddir = wp_upload_dir();
        $uploadfile = $uploaddir['path'] . '/' . basename($file['name'] );

        move_uploaded_file( $file['tmp_name'] , $uploadfile );
        $filename = basename( $uploadfile );

        $wp_filetype = wp_check_filetype(basename($filename), null );
        $_i = 0;

        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
            'post_content' => '',
            'post_status' => 'inherit',
            'menu_order' => $_i + 1000
        );
        $attach_id = wp_insert_attachment( $attachment, $uploadfile );

        $thumbnail_id = '';
        update_post_meta($event_id,'_thumbnail_id',$attach_id);
        set_post_thumbnail( $event_id, $thumbnail_id );
}

function cdashec_add_event_date_time($event_id, $event_all_day, $post_event_start_date, $post_event_end_date, $event_start_time, $event_end_time){
    $cde_helper = new Cdash_Events_Helper();
    $event_all_day = $event_all_day;
    $start_date_ok = false;

    update_post_meta($event_id, '_event_all_day', $event_all_day);
    $event_start_date = $event_end_date = $current_datetime = current_time('mysql', false);
    $current_date = date('Y-m-d', current_time('timestamp', false));

    // is it all day long event?
    if($event_all_day === 1){
        if($cde_helper->is_valid_date($post_event_start_date)){
            $start_date_ok = true;
            update_post_meta($event_id, '_event_start_date', $post_event_start_date.' 00:00:00');
            $event_start_date = $post_event_start_date.' 00:00:00';
        }else{
            update_post_meta($event_id, '_event_start_date', $current_datetime);
            $event_start_date = $current_datetime;
        }

        if($cde_helper->is_valid_date($post_event_end_date)){
            if($start_date_ok){
                if($cde_helper->is_after_date($post_event_end_date, $post_event_start_date)){
                    update_post_meta($event_id, '_event_end_date', $post_event_end_date.' 00:00:00');
                    $post_event_end_date = $event_end_date.' 00:00:00';
                }else{
                    $event_end_date = $event_start_date;
                    update_post_meta($event_id, '_event_end_date', $event_end_date);
                }
            }else{
                update_post_meta($event_id, '_event_end_date', $post_event_end_date.' 00:00:00');
                $event_end_date = $post_event_end_date.' 00:00:00';
            }
        }else{
            $event_end_date = $event_start_date;
            update_post_meta($event_id, '_event_end_date', $event_end_date);
        }
    }else{
        if($cde_helper->is_valid_date($post_event_start_date) && $cde_helper->is_valid_time($event_start_time)){
            $start_date_ok = true;
            $event_start_date = date('Y-m-d H:i:s', strtotime($post_event_start_date.' '.$event_start_time));
            update_post_meta($event_id, '_event_start_date', $event_start_date);
        }else{
            update_post_meta($event_id, '_event_start_date', $current_datetime);
            $event_start_date = $current_datetime;
        }

        if($cde_helper->is_valid_date($post_event_end_date) && $cde_helper->is_valid_time($event_end_time)){
            if($start_date_ok){
                if($cde_helper->is_after_date($post_event_end_date.' '.$event_end_time, $post_event_start_date.' '.$event_start_time)){
                    $event_end_date = date('Y-m-d H:i:s', strtotime($post_event_end_date.' '.$event_end_time));
                    update_post_meta($event_id, '_event_end_date', $event_end_date);
                }else{
                    $event_end_date = $event_start_date;
                    update_post_meta($event_id, '_event_end_date', $event_end_date);
                }
            }else{
                $event_end_date = date('Y-m-d H:i:s', strtotime($post_event_end_date.' '.$event_end_time));
                update_post_meta($event_id, '_event_end_date', $event_end_date);
            }
        }else{
            $event_end_date = $event_start_date;
            update_post_meta($event_id, '_event_end_date', $event_end_date);
        }
    }
}

function cdashec_add_event_ticket_details($event_id, $free_event, $tickets, $tickets_url, $tickets_url_name){
    update_post_meta($event_id, '_event_free', $free_event);

    if($free_event == 0){
        $ticket_url = $tickets_url;
        $ticket_url_name = $tickets_url_name;

        if(isset($tickets) && is_array($tickets) && !empty($tickets)){
            foreach($tickets as $id => $ticket){
                $tickets_fields = array();
                $empty = 0;

                foreach($tickets_fields as $key => $trans){
                    $tickets_fields[$key] = sanitize_text_field(isset($ticket[$key]) ? $ticket[$key] : '');
                    $empty += (($tickets_fields[$key] !== '') ? 1 : 0);
                }

                if($empty > 0)
                    $tickets[$id] = $tickets_fields;
            }

            if(empty($tickets)){
                $ticket_url = '';
                update_post_meta($event_id, '_event_free', 1);
            }
            update_post_meta($event_id, '_event_tickets', $tickets);
        }else{
            $ticket_url = '';

            update_post_meta($event_id, '_event_tickets', array());
            update_post_meta($event_id, '_event_free', 1);
        }

        update_post_meta($event_id, '_event_tickets_url', esc_url($ticket_url));
        update_post_meta($event_id, '_event_tickets_url_name', $ticket_url_name);
    }else{
        update_post_meta($event_id, '_event_tickets', $tickets);
        update_post_meta($event_id, '_event_tickets_url', '');
        update_post_meta($event_id, '_event_tickets_url_name', '');
    }
}
?>
