<?php
function cdashec_get_event_add_url(){
  $options = get_option('cdash_events_general');
  if(isset($options['event_update_url']) && $options['event_update_url'] !='' ){
    $event_add_url_id = $options['event_update_url'];
    $event_add_url_slug = get_post_field( 'post_name', $event_add_url_id );
    $event_add_url = home_url() . '/' . $event_add_url_slug;
  }else{
    $event_add_url = '';
  }
  return $event_add_url;
}

//Adding the event edit link to the member info display
//Check if MU is active
add_filter('cdashmm_member_info_filter', 'cdash_ec_add_event_link', 115, 2);
function cdash_ec_add_event_link($member_info, $business_id){
    $add_event_link = "";
    $options = get_option('cdash_events_general');
    if(cdash_check_mu_active()){
        if(isset($options['enable_member_event']) && $options['enable_member_event'] !=''){
            //get the link to the event update url from the event calendar options
        $event_add_url = '';
        $event_add_url = cdashec_get_event_add_url();
        $add_event_link = '<p><a href="' . $event_add_url . '">' . __('Add a new event', 'cdash-events') . '</a></p>';
        }
        
    }else{
        $add_event_link = "";
    }
    $member_info .= $add_event_link;
    return $member_info;
}

add_filter('cdashmm_member_info_filter', 'cdashec_display_event_edit_link', 117, 2);
function cdashec_display_event_edit_link($member_info, $business_id){
    if(function_exists('cdashmu_get_current_user_id')){
        $user_id = cdashmu_get_current_user_id();
    }else{
        $user_id = '';
    }

    if(function_exists('cdashmu_get_person_id_from_user_id')){
        $person_id = cdashmu_get_person_id_from_user_id($user_id, 'yes');
    }else{
        $person_id = '';
    }

    $event_edit_link = cdashec_get_event_link($person_id, 'yes');
    $member_info .= $event_edit_link;
    return $member_info;
}

function cdashec_get_event_link($person_id, $include_pending){
    if(!isset($events)){
        $events = "";
    }
    if($person_id == null){
        return null;
    }
    // Find connected events
    $connection_params = array(
	  'connected_type' => 'event_to_people',
	  'connected_items' => $person_id,
	  'nopaging' => true
	);

    if($include_pending){
        $connection_params['connected_query'] = array('post_status' => 'any');
    }
    $connected_events = new WP_Query( $connection_params);

    // Get the
    if ( $connected_events->have_posts() ) :
        $events .= __("<h4>Connected Events</h4>", "cdashec");
        $events .= "<ul class='connected_events'>";
        while ( $connected_events->have_posts() ) : $connected_events->the_post();
                $event_id = get_the_ID();
                $event_title = get_the_title();
                $event_permalink = get_the_permalink();
                //$add_event_url = cdashec_get_event_add_url();
                //$edit_event_url = $add_event_url . "/" . $event_id;
                //Enable this when we are ready to enable event edit option
                //$events .= "<a href='".$edit_event_url."'>".$event_title."</a><br />";
                $events .= "<li><a href='".$event_permalink."'>".$event_title."</a></li>";
        endwhile;
        $events .= "</ul>";
    else:
        //$event_id = null;
        $events = __("", "cdashec");
    endif;

    // Prevent weirdness
    wp_reset_postdata();

    return $events;
}

//Check if current user id is connected to the event id
function cdashec_connected_event_id($user_id, $event_id){
    $connected = '';
    //get the person connected to user id
    if(function_exists('cdashmu_get_person_id_from_user_id')){
        $person_id = cdashmu_get_person_id_from_user_id($user_id, 'yes');
    }else{
        $person_id = '';
    }

    //check if the person id is connected to the given event id
    //get the list of event id's connected to a person id
    if($person_id != ''){
        $event_ids = get_event_ids_connected_to_person_id($person_id, 'yes');
    }else{
        $event_ids = '';
    }
    //check if the current event id is in the list of connected event id's
    if(in_array($event_id, $event_ids)){
        $connected = "true";
    }else{
        $connected = "false";
    }
    $connected = $connected;
    return $connected;
}

function get_event_ids_connected_to_person_id($person_id, $include_pending){
    if($person_id == null){
        return null;
    }
    // Find connected events
    $connection_params = array(
	  'connected_type' => 'event_to_people',
	  'connected_items' => $person_id,
	  'nopaging' => true
	);

    if($include_pending){
        $connection_params['connected_query'] = array('post_status' => 'any');
    }
    $connected_events = new WP_Query( $connection_params);
    $event_ids = array();
    if ( $connected_events->have_posts() ) :
        while ( $connected_events->have_posts() ) : $connected_events->the_post();
                //$event_id = get_the_ID();
                $event_ids[] += get_the_ID();
        endwhile;
    else:
        //$event_id = null;
        $event_ids = "";
    endif;

    // Prevent weirdness
    wp_reset_postdata();

    return $event_ids;
}

function cdashec_display_connected_events(){
    //Check if user is logged in & get the user id

    //Check if there are any events connected to the user

    //get the connected event ids

    //display the event title with a link to view the event
}

function cdashec_send_admin_email($event_id, $business_id, $user_id, $additional_notes){
    $admin_subject = __( 'New event created!', 'cdashec' );

    $blog_title = get_bloginfo( 'name' );
    $admin_email = get_bloginfo('admin_email');


    $user_info = get_userdata($user_id);
    $first_name = $user_info->first_name;
    $last_name = $user_info->last_name;

    $business_name = get_the_title($business_id);
    $event_name = get_the_title($event_id);

    $admin_event_edit_link = admin_url().'post.php?post='.$event_id.'&action=edit';

    if($first_name && $last_name){
        $user = $first_name . ' ' . $last_name . __(' - an editor', 'cdashec');
    }else{
        $user = __('An editor', 'cdashec');
    }

    $admin_message = '<p>' . $user . __(' connected to ', 'cdashec') . $business_name . __(' has requested to add a new event to ', 'cdashec') . $blog_title . __('. You can click on the event link below to publish it. Once you publish, the event will show up on the events calendar and an email will be sent to the user.', 'cdashec');

    $admin_message .= '<p><a href="'.$admin_event_edit_link.'">' . __('Publish -', 'cdashec').$event_name.'</a></p>';
    $admin_message .= '<p><b>Additional Notes:</b><br />' . nl2br($additional_notes) . '</p>';

    cdashmm_send_email( $admin_email, $admin_email, '', $admin_subject, $admin_message );
}

function cdashec_send_user_email_on_event_publish($post_id) {
    if( isset($_POST['post_status']) && ( $_POST['post_status'] == 'publish' ) && ( $_POST['original_post_status'] != 'publish' ) ) {
        $blog_title = get_bloginfo( 'name' );
        $event = get_post($post_id);
        $event_title = $event->post_title;

        //check if first_name exists otherwise, use public display name
        $author_name = get_the_author_meta('first_name', $event->post_author );

        $admin_email = get_bloginfo('admin_email');
        $user_email = get_the_author_meta('user_email', $event->post_author );
        $user_id = get_the_author_meta('ID', $event->post_author);
        $user_info = get_userdata($user_id);

        $user_roles = implode(',', $user_info->roles);

        $email_subject = __( 'Your event has been published!', 'cdashec' );

        $email_message = '<p>'.__('Hello ', 'cdashec').$author_name .', </p>';
        $email_message .= '<p>'.__('Your event has been published on ', 'cdashec'). $blog_title.'. You can view the event on your account page. Here is the link to the published event: '.get_permalink( $post_id ).'</p>';
        if(!empty($user_roles)){
            if(is_array($user_roles)){
                if(in_array('cdashmu_business_editor', $user_roles)){
                    cdashmm_send_email( $admin_email, $user_email, '', $email_subject, $email_message );
                }
            }else{
                if($user_roles == 'cdashmu_business_editor'){
                    cdashmm_send_email( $admin_email, $user_email, '', $email_subject, $email_message );
                }
            }
        }
       //cdashmm_send_email( $admin_email, $user_email, '', $email_subject, $email_message );
       flush_rewrite_rules();
    }
}

add_action( 'publish_event', 'cdashec_send_user_email_on_event_publish' );

?>
