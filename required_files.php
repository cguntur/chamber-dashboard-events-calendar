<?php
if ( ! defined('ABSPATH') ) {
	die('Please do not load this file directly.');
}
foreach ( glob( plugin_dir_path( __FILE__ ) . "includes/*.php" ) as $file ) {
    require_once $file;
}

foreach ( glob( plugin_dir_path( __FILE__ ) . "member-events/*.php" ) as $file ) {
	require_once $file;
}

foreach ( glob( plugin_dir_path( __FILE__ ) . "blocks/*.php" ) as $file ) {
	require_once $file;
}

?>
