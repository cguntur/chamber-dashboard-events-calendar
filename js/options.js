jQuery(document).ready(function($) {
    $('#em_events_overlay_bck_color').wpColorPicker();
    $('#em_events_overlay_text_color').wpColorPicker();

    $('#em_enable_member_event_update').ec_show_fields_if_checked('#em_update_events_url');
    // $('#enable_user_registration').show_fields_if_checked('#cdashmm_login_logout_link');
   
    $('#em_enable_member_event_update').change(function() {
        //alert("Hello!");
        $(this).ec_show_fields_if_checked('#em_update_events_url');
        //$(this).show_fields_if_checked('#cdashmm_login_logout_link');
    });

});

(function($){
    $.fn.ec_hide_fields_if_checked = function(selector) {
      if((this).is(":checked")){
          alert("One");
        $(selector).parent().parent().hide("slow");
        $(selector).prop('disabled', 'disabled');
      }else{
          alert("two");
        $(selector).parent().parent().show("slow");
        $(selector).prop('disabled', false);

      }
    }

    $.fn.ec_show_fields_if_checked = function(selector) {
      if((this).is(":checked")){
          //alert("three");
        $(selector).parent().parent().show("slow");
        $(selector).prop('disabled', false);
      }else{
          //alert("four");
        $(selector).parent().parent().hide("slow");
        $(selector).prop('disabled', 'disabled');
      }
    }
})(jQuery);
