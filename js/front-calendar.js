document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('events-full-calendar');
    var showPopup = showEventPopup();
    var calendar = new FullCalendar.Calendar(calendarEl, {
        locale: emCalendarArgs.locale,
        buttonIcons: true,
        navLinks: true,
    	header: {
    		left: 'prev,next,today',
    		center: 'title',
    		right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
    	},
        plugins: [ 'dayGrid', 'timeGrid', 'list', 'moment' ],
        defaultView: emCalendarArgs.defaultView,
        firstDay: +emCalendarArgs.firstDay, //added the '+' in front of the variable so that the firstDay value is converted to a number
        eventRender: function(info) {
            if(showPopup){
                var tooltip = new Tooltip(info.el, {
                    title: cdashEcTitle(info.event.title) + cdashEcDate(info.event.start, info.event.end, info.event.allDay),
            		html: true,
                    placement: 'top',
                    trigger: 'hover',
                    container: 'body'
                });
            }
        },
    	events: emCalendarArgs.events,
    });

    calendar.render();
});

function showEventPopup() {
    var popupEnabled = document.querySelector('#events-full-calendar');
	if (popupEnabled.classList.contains('popup_enabled')) {
        return true;
	}else{
        return false;
    }
}

function cdashEcTitle(title){
    var newTitle = '<h6 class="event_title">' + title + '</h6>';
    return newTitle;
}

function cdashEcDate(startDate, endDate, allDay){
    var startDate = new Date(startDate);
    if(endDate !== null){
        var endDate = new Date(endDate);
    }else{
        var endDate = startDate;
    }
    if(allDay){
        endDate.setDate(endDate.getDate() - 1);
    }
    var newStartDate = startDate.toDateString();
    var newEndDate = endDate.toDateString();

    var newStartDay = startDate.getDay();
    var newStartMonth = startDate.getMonth();
    var newStartYear = startDate.getFullYear();

    var newEndDay = endDate.getDay();
    var newEndMonth = endDate.getMonth();
    var newEndYear = endDate.getFullYear();

    var displayDate = "";

    var startHours = startDate.getHours() > 12 ? startDate.getHours() - 12 : startDate.getHours();
    var startMinutes = addZero(startDate.getMinutes());
    var startAmPm = startDate.getHours() >= 12 ? "pm" : "am";

    var endHours = endDate.getHours() > 12 ? endDate.getHours() - 12 : endDate.getHours();
    var endMinutes = addZero(endDate.getMinutes());
    var endAmPm = endDate.getHours() >= 12 ? "pm" : "am";

    if(newStartDay == newEndDay && newStartMonth == newEndMonth && newStartYear == newEndYear){
        if(allDay == true){
            displayDate += "<span>" + newStartDate + "</span><br />";
        }else if(startHours == endHours && startMinutes == endMinutes && startAmPm == endAmPm){
            displayDate += "<span>" + newStartDate + "</span><br />";
            displayDate += "<span>" + startHours + ":" + startMinutes + " " + startAmPm + "</span><br />";
        }else{
            displayDate += "<span>" + newStartDate + "</span><br />";
            displayDate += "<span>" + startHours + ":" + startMinutes + " " + startAmPm + " - " + endHours + ":" +  endMinutes + " " + endAmPm + "</span><br />";
        }
    }else{
        if(allDay == true){
            displayDate += "<span>" + newStartDate + " - " + newEndDate + "</span><br />";
        }else{
            displayDate += "<span>" + newStartDate + " - " + newEndDate + "</span><br />";
            displayDate += "<span>" + startHours + ":" + startMinutes + " " + startAmPm + " - " + endHours + ":" +  endMinutes + " " + endAmPm + "</span><br />";
        }
    }
    return displayDate;

}

function addZero(i) {
  if (i < 10) {
    i = "0" + i;
  }
  return i;
}
