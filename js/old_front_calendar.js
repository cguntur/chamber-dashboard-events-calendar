jQuery(document).ready(function($) {
	$('#events-full-calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay,listMonth'
		},
		views: {
				listMonth: {
						noEventsMessage: 'No events to display this month, try advancing to next month!',
				}
		},
		timeFormat: emCalendarArgs.timeFormat,
		firstDay: emCalendarArgs.firstWeekDay,
		events: emCalendarArgs.events,
		editable: false,
		defaultView: emCalendarArgs.defaultview,
		eventMouseover: function (event, jsEvent) {
			if($('#events-full-calendar').hasClass('popup_enabled')){
				var startTime = moment(event.start).format('MMMM Do YYYY');
	        $(this).popover({
	            title: event.name,
				html: true,
	            placement: 'top',
	            trigger: 'hover',
	            content: '<div class="cdash_events_popover" title="Event Details"><h6 class="cdash_events_popover_title">'+event.title+'</h6><span id="startTime">'+startTime+'</span><br /></div>',
	            container: '#events-full-calendar'
	        }).popover('show');
			}
		},
		displayEventTime: false
  });
});
